
#ifndef MODBUSTCP_H
#define MODBUSTCP_H

#include "main.h"
#include "cmsis_os.h"


#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "cmsis_os.h"
#include "ethernetif.h"
#include "app_ethernet.h"


extern uint8_t GIpAddrConfigured;

//#define USE_DHCP       /* enable DHCP, if disabled static address is used*/

/*Static IP ADDRESS*/
#define IP_ADDR0   192
#define IP_ADDR1   168
#define IP_ADDR2   0
#define IP_ADDR3   201
   
/*NETMASK*/
#define NETMASK_ADDR0   255
#define NETMASK_ADDR1   255
#define NETMASK_ADDR2   255
#define NETMASK_ADDR3   0

/*Gateway Address*/
#define GW_ADDR0   192
#define GW_ADDR1   168
#define GW_ADDR2   0
#define GW_ADDR3   1 

typedef struct
{
	uint32_t byte1;
	uint32_t byte2;
	uint32_t byte3;
	uint32_t byte4;
} TIpAddr;

extern TIpAddr GIpAddress;
extern TIpAddr GNetMask;
extern TIpAddr GDefaultGateway;


extern void ModbusTCPThread(void const *argument);


#endif

