/**
  ************************************************************************************************
  * @file	   ApplicationCommon.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    20-December-2016
  * @brief   Set here Number of channels!
	*					 Set here parameters 
  *************************************************************************************************
  */
#ifndef APPLICATION_COMMON_H
#define APPLICATION_COMMON_H

#include "main.h"
#include "cmsis_os.h"
#include "I2C/ds18b20.h"
#include "TagSystem/TagSystem.h"


#endif

