/**
  ************************************************************************************************
  * @file	   Application.h
  * @author  Kortev Oleg
  * @version V3.2
  * @date    2016
  * @brief   Set here Number of channels!
	*					 Set here parameters of Taho structure, local maximums function and size of logs buffer.
	*						and field.
  *************************************************************************************************
  */
#ifndef APPLICATION_H
#define APPLICATION_H
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "cmsis_os.h"

#include "I2C/ds18b20.h"
#include "OvalSensors.h"

// -------------------------------------------------------- Exported define ----------------------:
#define TAHO_BUF_SIZE 10
#define TAHO_UPDATE_PERIOD 100					// ms

#define LOG_OUTPUT_VALUE_PERIOD	5000			// ms

#define USE_LOCAL_MAXIMUMS					// define, if dynamic top values calculation is needed.
#define TOP_VALUE_UPDATE_PERIOD 500			// ms

// Log strings(lines) are puted to circular buffer =====:
#define LOG_LINES_BUFF_SIZE  32		

#define VALVE_CNT 28

// -------------------------------------------------------- Exported types -----------------------: 
// Structure for Tahometer sensor =========================================:
typedef struct
{
	uint32_t FPulse; 					// pulse from taho sensor FLG	
//	uint32_t FCnrBuffer[10]; 	// pulses counter
	uint8_t  FBufferPos;  			// current position in buffer for new data 	
	uint32_t FTimeCnr;					// current period of pulses on taho sensor
	uint32_t FTimeBuffer[10]; 	// contains values of FTimeCnr
//	uint8_t  FTimeBufferPos;  
	
	float FOutput; 							// output value in RPM
} 
TTaho;		

// Structure for logs ===========================================================================:
typedef struct
{
	char 		line[LOG_LINES_BUFF_SIZE][32];			// Cyclic buffer for log strings.
	uint8_t	curr_line;				// Number of last written line.
} TLog;

// Structure for Sensor Channel =================================================================:
typedef struct
{
	char *FChannelName;  		// name of measure channel
	
	OWI_device *FTemperatureSensor1;			// pointer to DS18B20 temperature 1 sensor (device input)
	uint8_t FTemperatureSensor1Ready;			// ready of temperature sensor initiates temperature sensor visibility
	OWI_device *FTemperatureSensor2;			// pointer to DS18B20 temperature 2 sensor
	uint8_t FTemperatureSensor2Ready;		  // ready of temperature sensor initiates temperature sensor visibility
	
	TOval *FOval; 											  // pointer to oval sensor structure
	
	float FOutputTemperature1;						// output temperature	1  
	float FOutputTemperature2;						// output temperature 2
	float FOutputValue[UNITS_CNT];			  // output value in different units
	float	FOutputValueDiscovery[UNITS_CNT];	// Output value for Discovery Display.

	uint8_t FUnitId;            				// output value units idetifier	
	float FTopValue; 										// top output value for on-screen display
	uint8_t FChannelError;							// error of the channel
	uint8_t FMeasureStartTimer;					// timer for first pulse skipping
	
	TLog	Log;								// log
} 
TChannel;

// valve structure =============================:
typedef struct
{
	uint8_t FValue;
} 
TValve;   // valve structure

// Structure for GValvesMode ======================================================================:
typedef enum
{
	DELIVERY_MODE = 0x0000,
	BACK_FLOW_MODE = 0x0001,
	BOTH_TURNED_ON = 0x0002,
	BOTH_TURNED_OFF = 0x0003,
	DUMMY_MODE  = 0xFFFF
}	
TValvesMode;

// -------------------------------------------------------- Exported variables -------------------:
extern TTaho GTaho;  				// taho sensor
extern TChannel GChannel[CHANNEL_NUM];		// Sensors Channels.
extern TValve GValve[VALVE_CNT]; 

extern uint8_t GMeasurementsON;			// MeasurementsON FLG for displaying output values on discovery screen.
extern uint32_t	GDeviceWorkingTime;	// Time from first calculation start.

// Modbus discrete coils:
extern bool GApplicationRun;			// Command to start measurement
extern bool GApplicationStop;			// Command to stop measurement
extern bool GApplicationMeasurementReady;	
// Modbus holding register:
extern uint32_t GOvalBuffSize;			// Max size of Oval sensor buffer 1..512.
extern uint16_t GFlashMapSize;			
extern uint32_t GFanON_Temperature;	// Critical value of themperature.	

// -------------------------------------------------------- Exported functions -------------------: 
extern void ApplicationThread(void const *argument);	

extern void SetOutputMaximums(void);		// Calculate top channel values for displaing in flasks.
extern void ResetOutputValues(TChannel* channel);		// Reset output channel values and clear oval sensor buffer. 




#endif


