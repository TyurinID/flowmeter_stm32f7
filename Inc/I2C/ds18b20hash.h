
#ifndef DS18B20_HASH
#define DS18B20_HASH

#include "main.h"
#include "cmsis_os.h"
#include "I2C/ds18b20.h"

#define MAXDS18B20HASHSIZE 20


typedef struct
{
	uint8_t FId[8]; // 64 bit idetifier
//	uint32_t FNetAddr;  // net adress setted in SensorDLG.
} TDS18B20_HashFile;

extern uint8_t	GSettedNetAddr[];			// Thermometers Net Addresses setted in SensorDLG.

extern uint16_t GFlashMapSize;

extern TDS18B20_HashFile GDS18B20_HashFile[MAXDS18B20HASHSIZE];

extern uint8_t FindDs18b20NetAddr(uint8_t *id);  // find sensor in hash by rom code

extern uint8_t SaveDs18b20InHash(uint8_t *id,  uint8_t NetAddr);  // save new field in hash

extern void	SaveAllDs18b20(void);		// Save new NetAddresses in flash.

#endif
