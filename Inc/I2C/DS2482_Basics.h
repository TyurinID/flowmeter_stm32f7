
#ifndef DS2482_BASIC_H
#define DS2482_BASIC_H

#include "main.h"
#include "cmsis_os.h"



extern uint8_t DS2482_OWI_ChannelSelect(uint8_t bus); //  select owi channel
extern uint8_t DS2482_RST(void); // reset DS2482
extern uint8_t DS2482_OWI_RST(void);    // reset selected one wire port
extern uint8_t DS2482_ReadStatus(void); // read status register of DS2482
extern uint8_t DS2482_OWI_SINGLEBIT_WRITE(uint8_t bit); // write single bit to selected owi bus
extern uint8_t DS2482_OWI_SINGLEBIT_READ(void);  // read single bit from owi device
extern uint8_t DS2482_OWI_Busy(void);  // read if selected one-wire bus is busy
extern uint8_t DS2482_OWI_TRIPLET(uint8_t direction);  // write triplet sequence to the selected bus

#endif
