
#ifndef DS18B20_H
#define DS18B20_H

#include "DS2482_Basics.h"
#include "ds18b20hash.h"

#define MAX_DS18B20_DEVICES 18					// 18 

#define DS18B20_SENSORS_PER_BUS_0 10
#define DS18B20_SENSORS_PER_BUS_1 8

typedef struct
{
  uint8_t bus;  // bus number
	uint8_t id[8]; // 64 bit idetifier
	uint8_t NetAdr; // network adress
	int16_t temperature;  // sensor temperature
	float		CTemperature;	// Calculated temperature. 
	
	uint8_t NotConfigured;
	uint8_t Iteration_cnt;
} OWI_device;

extern OWI_device ds18b20_devices[MAX_DS18B20_DEVICES];
extern uint8_t G_ds18b20_Count;


extern void OWI_Write(uint8_t data); // write data to selected bus
extern uint8_t OWI_Read(void);  // read data from selected bus

extern uint8_t DS18B20_StartConversion(void);     // start conversion on selected bus
extern int16_t DS18B20_ReadTemperature(void);  // read temperature from the selected sensor in the selected bus

extern uint8_t OWI_CRC(uint8_t *data, uint8_t len);  // compute owi crc

extern void ClearDS18B20List(void);   // clear sensors list
extern void GenerateDS18B20List(void);  // search sensors and generate list
extern uint8_t SearchBus(OWI_device *devices, uint8_t len, uint8_t bus);	 // Search len sensors on bus. Returns 0x02, if there aren't any sensors. 
 
extern int16_t DS18B20_ReadTemperatureFromRomCode(uint8_t *RomCode); // read temperature from selected rom code on selected bus

extern uint8_t DS18B20_ReadNetAdrFromRomCode(uint8_t *RomCode); // read net adress from selected rom code on selected bus

extern uint8_t DS18B20_WriteNetAddr(uint8_t Number, uint8_t NetAddr); // write new net address to the sensor

extern void sort_sensors(void);

extern float get_sensor_temperature(OWI_device device);

extern uint8_t get_sensor_not_ready(OWI_device device);

#endif
