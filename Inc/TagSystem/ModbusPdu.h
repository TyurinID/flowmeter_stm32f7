
#ifndef MODBUS_PDU_H
#define MODBUS_PDU_H

#include "TagSystem/TagSystem.h"

extern void MakeModbusPdu(uint8_t *rec_buf, uint16_t rec_buf_size, uint8_t *transmit_buf, uint16_t *tr_buf_size);

#endif

