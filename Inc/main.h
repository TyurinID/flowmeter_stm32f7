/**
  ************************************************************************************************
  * @file	 	 main.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    20-December-2016
  * @brief   Header for main.c.
  * 				 Define: threads delays;
	*									 threads periods;
	*									 number of using channels;
  *************************************************************************************************
  */
#ifndef __MAIN_H
#define __MAIN_H

// -------------------------------------------------------- Includes -----------------------------: 
#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_sdram.h"
#include <stdbool.h>
#include "GUI.h"

// -------------------------------------------------------- Exported define ----------------------:
// Threads Delays for thermometers to be initialiazed (in I2C Thread) =========:
#define 	APPLICATION_THREAD_DELAY			5500		//ms
#define 	GRAPHIC_THREAD_DELAY					6000		//ms

// Threads periods =========================================:
#define APPLICATION_PERIOD_MS			10


// Number of using channels =============:
#define CHANNEL_NUM 10


// Numer of units: ml/min, ml/10_str, ...
#define UNITS_CNT 5


// -------------------------------------------------------- Exported variables -------------------: 
extern char GUnitNames[UNITS_CNT][15];
extern char GChannelNames[CHANNEL_NUM][15];

#endif /* __MAIN_H */


/**********************************************************************************END OF FILE****/ 
