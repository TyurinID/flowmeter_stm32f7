/**
  ************************************************************************************************
  * @file	   GraphKeyboard.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for GraphKeyboard.c.
	*					 Set here coordinates of Keyboard.
  *************************************************************************************************
  */
#ifndef GRAPH_KEYBOARD_H
#define GRAPH_KEYBOARD_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"


// -------------------------------------------------------- Exported define ----------------------:
#define KEY_BOARD_DIGITS 11

#define BCKSP_POS 10
#define ENTER_POS 11

// Keboard coordinates =============:
#define COLUMN1_X0	195
#define COLUMN1_X1	283
#define COLUMN2_X0	286
#define COLUMN2_X1	373
#define COLUMN3_X0	376
#define COLUMN3_X1	465

#define LINE1_Y0		20
#define LINE1_Y1		62
#define LINE2_Y0		65
#define LINE2_Y1		107
#define LINE3_Y0		110
#define LINE3_Y1		152
#define LINE4_Y0		155
#define LINE4_Y1		200


// -------------------------------------------------------- Exported types -----------------------:
typedef enum
{
  KB_NORMAL  = 0, 
  KB_DRAW_FRAME,
	KB_DRAW_ADDIT
} TKbMode;

typedef struct
{
	uint32_t 			*Target;					// General value to be changed and displayed.
	float		 			*AdditTarget;			// Additional value to be displayed.
	uint8_t 			digits[KEY_BOARD_DIGITS];		// Numbers of general value.
	uint8_t 			digits_length;		// Current numer of digits.
	
	uint16_t			Coord[4];		// x0, x1, y0, y1 there value should be displayed.
	uint8_t 			Active;  		// Keyboard Active FLG.
	TKbMode				Mode;				// mode: normal/with_frame/with_additional_trget.
	uint32_t 			FMin;				// min value
	uint32_t 			FMax;  			// max value	
} TKeyBoard;



// -------------------------------------------------------- Exported variables -------------------:
extern TKeyBoard GKeyBoard;

// -------------------------------------------------------- Exported functions -------------------:
extern void KeyBoard_Init(void);			// Keyboard characteristics on the screen
extern void KeyBoard_Draw(void);			//  
extern void KeyBoard_Process(__IO TS_StateTypeDef  ts);		// TI: 



#endif
