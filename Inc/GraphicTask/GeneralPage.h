/**
  ************************************************************************************************
  * @file	   GeneralPage.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for GeneralPage.c.
  *************************************************************************************************
  */

#ifndef GENERAL_PAGE_H
#define GENERAL_PAGE_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"


extern void GeneralPage_Draw(void);
extern void GeneralPage_Process(__IO TS_StateTypeDef  ts);

#endif	/*GENERALPage_H*/
