/**
  ************************************************************************************************
  * @file	   ScreenSectors.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for ScreenSectors.c.
	*					 Set here number of GScreenSectors.
  *************************************************************************************************
  */
#ifndef SCREEN_SECTORS_H
#define SCREEN_SECTORS_H

// -------------------------------------------------------- Includes -----------------------------:
#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include "cmsis_os.h"

// -------------------------------------------------------- Exported types -----------------------:
typedef struct
{
	int FXBegin;
	int FXEnd;
	int FYBegin;
	int FYEnd;
} TScreenSector;

// -------------------------------------------------------- Exported define ----------------------:
// Screen devided into sectors to display information by parts ===:
#define SCREEN_SECTORS_NUM 6		

// todo: define screen sectors coordinates!

// -------------------------------------------------------- Exported variables -------------------:
extern TScreenSector GScreen_sector[SCREEN_SECTORS_NUM];

// -------------------------------------------------------- Exported functions -------------------:
extern void InitializeScreenSectors(void);

#endif

