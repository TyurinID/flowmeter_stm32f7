/**
  ************************************************************************************************
  * @file	   ChannelPage.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for ChannelPage.c.
  *************************************************************************************************
  */
	
#ifndef CHANNEL_PAGE_H
#define CHANNEL_PAGE_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"


extern void ChannelPage_Draw(uint8_t page_number);
extern void ChannelPage_Process(__IO TS_StateTypeDef  ts, uint8_t page_number);

#endif	/*GENERAL_PAGE_H*/


