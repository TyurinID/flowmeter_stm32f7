/**
  ************************************************************************************************
  * @file	   ThermometerWindow.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for ThermometerWindow.c.
	*					Set here coordinates of all labels and fields.
  *************************************************************************************************
  */
	
#ifndef THERMOMETERS_WIN_H
#define THERMOMETERS_WIN_H

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"
#include "GUI.h"
#include "stm32746g_discovery_ts.h"

// -------------------------------------------------------- Exported types -----------------------:
typedef	struct	{
	uint16_t	x0;
	uint16_t	x1;
	uint16_t	x2;
	uint16_t	y0;
	uint16_t	y1;
}		TThermoField;

// -------------------------------------------------------- Exported define ----------------------:
// Table coordinates =======:
#define	TABLE_X0	50
#define	WIDTH_1		47
#define	WIDTH_2		50

#define	TABLE_Y0	10
#define	HEIGHT		35

// -------------------------------------------------------- Exported functions -------------------:
extern void ThermometersWin_Init(void);
extern void ThermometersWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts);

extern void	SaveAllDs18b20(void);		// Save new NetAddresses in flash.








#endif

