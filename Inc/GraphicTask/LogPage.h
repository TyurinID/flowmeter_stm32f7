/**
  ************************************************************************************************
  * @file	   LogPage.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for LogPage.c.
	*					Set here coordinates of all labels and fields.
  *************************************************************************************************
  */
	
#ifndef LOGDLG
#define LOGDLG

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include "Application.h"

// -------------------------------------------------------- Exported define ----------------------:
				
// Log strings(lines) are displayed in table ===========:
#define LOG_LINES_NUM  			24

// -------------------------------------------------------- Exported functions -------------------:

extern void AddLog(TChannel* channel, char* text);	// Add log string in log table for displaing on LogPage.

extern void LogPage_Init(void);
extern void LogPage_Draw(TChannel* first_ch, TChannel* second_ch);
extern void LogPage_Process(__IO TS_StateTypeDef  ts);

#endif


