/**
  ************************************************************************************************
  * @file	 		GraphicTask.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   
  *************************************************************************************************
  */

#ifndef GRAPHICTASK_H
#define GRAPHICTASK_H

// -------------------------------------------------------- Includes -----------------------------:
#include "cmsis_os.h"

// -------------------------------------------------------- Exported define ----------------------:
// FlowMeter_Colors =======:
#define		FM_COLOR_BK1				0x7b5d43
#define		FM_COLOR_BK2				0x917964		// #define		FM_COLOR_BK2				0x896f59
#define		FM_COLOR_DARKBLUE		0x4f2d12
#define		FM_COLOR_WHITE			0xece8e1
#define		FM_COLOR_GREY				0xc5c5c5

// -------------------------------------------------------- Exported types -----------------------:
typedef enum {
	MAIN_WIN,
	SETTINGS_WIN,
	THERMOMETERS_WIN
}	TActiveWindow;

// -------------------------------------------------------- Exported variables -------------------:
extern TActiveWindow GActiveWindow; 							// Number of current window to be displaed.

// -------------------------------------------------------- Exported functions -------------------:
extern void GraphicThread(void const *argument);    	// graphic task for main application


#endif

