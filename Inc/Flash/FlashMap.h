
#ifndef FLASH_MAP_H
#define FLASH_MAP_H

#include "main.h"
#include "cmsis_os.h"


#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base address of Sector 0, 32 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08008000) /* Base address of Sector 1, 32 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08010000) /* Base address of Sector 2, 32 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x08018000) /* Base address of Sector 3, 32 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08020000) /* Base address of Sector 4, 128 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08040000) /* Base address of Sector 5, 256 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08070000) /* Base address of Sector 6, 256 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x080A0000) /* Base address of Sector 7, 256 Kbytes */



#define FLASH_USER_START_ADDR     0x080D0000 /* Base address of Sector 6, 256 Kbytes */
#define FLASH_USER_END_ADDR     ADDR_FLASH_SECTOR_7 /* Base address of Sector 7, 256 Kbytes */


#define FLASHMAP_COUNT 1024


typedef struct
{
	uint32_t *FTarget;
	uint32_t FBufferValue;
} TFlashSavingRegister;

extern TFlashSavingRegister GFlashSavingRegister[FLASHMAP_COUNT]; // flash data map



extern uint8_t SaveNewFlashMap(uint16_t size);

extern void RecallFlashMap(uint16_t size);

extern uint8_t CheckFlashMap(uint16_t size);

#endif

