/**
  ************************************************************************************************
  * @file	   OvalSensors.h
  * @author  Kortev Oleg
  * @version V3.2
  * @date    2016
  * @brief   Set here Number of channels!
	*					 Set here parameters of Taho structure, local maximums function and size of logs buffer.
	*						and field.
  *************************************************************************************************
  */
#ifndef OVAL_SENSORS_H
#define OVAL_SENSORS_H

// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"


// -------------------------------------------------------- Exported define ----------------------:
#define OVAL_SENSORS_CNR 			14			// all channels use oval_sensors DO NOT CHANGE!!
#define OVAL_BUF_MAX_SIZE 		512
#define OVAL_MAX_CALIBRATION 	999999
#define OVAL_COUNT_DELAY_MS 	1000
#define OVAL_MAX_PERIOD				3000000

// -------------------------------------------------------- Exported types -----------------------: 
typedef struct
{
	uint32_t FPulse; 			// pulse from taho sensor FLG	
	
//	uint32_t FBuffer[OVAL_BUF_MAX_SIZE];	
	uint16_t FBufCnt;
//	uint64_t FBufSum;	
	
	uint32_t FTimes_cnr;			// Time between two pulses in 10*ms.
	uint32_t FTimesBuffer[OVAL_BUF_MAX_SIZE];			// Time buffer in 10*ms.
	uint16_t FTimesBufCnt;
	float		 FAverageTime;					// Calculated time in 10*ms.
	
	uint32_t FCalibration; // calibration parameter
	
	uint8_t FReady;  			// ready FLG	
	uint8_t FMeasureLatch; // measurre latch	
	uint8_t FFirstCycle; // first cycle flag	
	uint32_t	FMeasurementTime; 	// oval sensor measuring time in seconds. TI? NOT USING	
} TOval;

// -------------------------------------------------------- Exported variables -------------------: 
extern TOval GOval[OVAL_SENSORS_CNR];  // oval sensors

// -------------------------------------------------------- Exported functions -------------------: 
extern void OvalInit(void); 					// init oval sensor
extern void UpdateOval(TOval *oval);  // update oval value
extern float CountOvalOutput(TOval *oval, uint8_t unit_id, float taho); 	// count output value
extern void ResetOval(TOval *oval);  // reset oval

extern uint8_t OvalSensorReady(TOval *oval);

#endif


