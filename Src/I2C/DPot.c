
#include "I2C/DPot.h"

#include "I2C/I2C_Basic.h"

#define DPOT_I2C_ADDR (uint8_t)0x5e    // DPot slave address
 
static uint8_t aTxBuffer = 0x0;  // 0x7f = maximum  (~5 V)
																	// 0x33 = 12 V
static uint8_t aRxBuffer;

#define EN_PORT GPIOA
#define EN_PIN GPIO_PIN_15

#define BYP_PORT GPIOI
#define BYP_PIN GPIO_PIN_2

void DPotInit(void)
{
	I2C_Ready = 0;
	
	uint8_t dpot_ready = 0;
	
	while(0==dpot_ready)
	{
		while(TI_HAL_I2C_Master_Transmit(&I2cHandle, DPOT_I2C_ADDR, &aTxBuffer, 1, I2C_TIMEOUT) != HAL_OK) 
			osDelay(10);
		
		while(TI_HAL_I2C_Master_Receive(&I2cHandle, DPOT_I2C_ADDR, &aRxBuffer, 1, I2C_TIMEOUT) != HAL_OK) 
			osDelay(10);
		
		if(aTxBuffer == aRxBuffer) // verify
		{
			dpot_ready = 1;
		}
	}
	
	// en pin
	GPIO_InitTypeDef EN_PinInit;
	
	EN_PinInit.Pin = EN_PIN;
	EN_PinInit.Mode = GPIO_MODE_OUTPUT_PP;
	EN_PinInit.Pull = GPIO_NOPULL;
	EN_PinInit.Speed = GPIO_SPEED_FAST;
	
	HAL_GPIO_Init(EN_PORT, &EN_PinInit);
	
	HAL_GPIO_WritePin(EN_PORT, EN_PIN, GPIO_PIN_SET);
	
	
	// bypass pin
	osDelay(10);
	__GPIOI_CLK_ENABLE();
	GPIO_InitTypeDef Byp_PinInit;
	
	Byp_PinInit.Pin = BYP_PIN;
	Byp_PinInit.Mode = GPIO_MODE_OUTPUT_PP;
	Byp_PinInit.Pull = GPIO_NOPULL;
	Byp_PinInit.Speed = GPIO_SPEED_FAST;
	
	HAL_GPIO_Init(BYP_PORT, &Byp_PinInit);
	
	HAL_GPIO_WritePin(BYP_PORT, BYP_PIN, GPIO_PIN_SET);
	
}
