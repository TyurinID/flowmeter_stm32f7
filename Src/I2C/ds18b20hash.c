
#include "I2C/ds18b20hash.h"

#include "FLash/FlashMap.h"

#include <string.h>


TDS18B20_HashFile GDS18B20_HashFile[MAXDS18B20HASHSIZE];
uint8_t		GSettedNetAddr[MAX_DS18B20_DEVICES];


uint8_t FindDs18b20NetAddr(uint8_t *id)  // find sensor in hash by rom code
{
	for(uint8_t i=0; i<MAXDS18B20HASHSIZE; i++)
	{
		uint8_t match = 1;
		for(uint8_t j = 0; j<8; j++)
		{
			if(id[j] != GDS18B20_HashFile[i].FId[j])
				match = 0;
		}
		
		if(match)
		{
			return (i+1);
		}
		
	}
	
	
	return 0xff;
}

uint8_t SaveDs18b20InHash(uint8_t *id,  uint8_t NetAddr)
{
	if(MAXDS18B20HASHSIZE<NetAddr || 0==NetAddr)
	{
		return 0;
	}
	
	NetAddr--;
	memcpy(GDS18B20_HashFile[NetAddr].FId, id, 8);
	
	SaveNewFlashMap(GFlashMapSize);
	GDS18B20_HashFile[NetAddr].FId[0] = 0;
	RecallFlashMap(GFlashMapSize);
	
	return (NetAddr);
}

void	SaveAllDs18b20(void)
{
	for(uint8_t position=0; position<MAX_DS18B20_DEVICES; position++)
		if(ds18b20_devices[position].NetAdr-1 < MAXDS18B20HASHSIZE)
			memcpy(GDS18B20_HashFile[ds18b20_devices[position].NetAdr-1].FId, ds18b20_devices[position].id, 8);		

	SaveNewFlashMap(GFlashMapSize);
	
	for(uint8_t position=0; position<MAX_DS18B20_DEVICES; position++)
		GDS18B20_HashFile[position].FId[0] = 0;
	
	RecallFlashMap(GFlashMapSize);
}


















