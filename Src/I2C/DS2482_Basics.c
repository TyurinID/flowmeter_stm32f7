
#include "I2C/DS2482_Basics.h"

#include "I2C/I2C_Basic.h"

// DS2482-800 definitions
static const uint8_t TSB = 0x40;
static const uint8_t SBR = 0x20;
static const uint8_t PPD = 0x02;
static const uint8_t WB  = 0x01;


#define DS2482_I2C_ADDR (uint8_t)0x30    // ds2482 slave address

static uint8_t aTxBuffer[2] = {0xf0, 0x30};
static uint8_t aRxBuffer[2];

// DS2482-800 functions
uint8_t DS2482_RST(void)	// reset ds2842 device
{	
	// transmiting slave adress and reset command
	aTxBuffer[0] = 0xf0;  // reset command
	
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 1, I2C_TIMEOUT)!= HAL_OK) 
		return 0;			// if error.
	else
		return 1;			// if NO error.
	
//	osDelay(1);
//	while(0==I2C_Ready) 
//		osDelay(1);
//		
//	
//	if (HAL_I2C_GetError(&I2cHandle) == HAL_I2C_ERROR_NONE)
//		return 1;
//	else
//		return 0;
}

uint8_t DS2482_ReadStatus(void) // read status register of DS2482
{
	// first we set read pointer to the correct position (status reg)
	aTxBuffer[0] = 0xe1; // transmiting set read pointer command
	aTxBuffer[1] = 0xf0; // transmiting read pointer to status register
	
	I2C_Ready = 0;
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 1, I2C_TIMEOUT)!= HAL_OK) 
		return 0xff;
	
	if(TI_HAL_I2C_Master_Receive(&I2cHandle, DS2482_I2C_ADDR, (uint8_t *)aRxBuffer, 1, I2C_TIMEOUT) != HAL_OK) 
		return 0xff;
	
	osDelay(2);			// TI! for freertos
	
	return aRxBuffer[0];
}

uint8_t DS2482_OWI_ChannelSelect(uint8_t bus)
{	
	aTxBuffer[0] = 0xc3; // transmiting command code
	switch(bus) // chanel select
	{
		case 0: aTxBuffer[1] = 0xf0; break;
		case 1: aTxBuffer[1] = 0xe1; break;
		case 2: aTxBuffer[1] = 0xd2; break;
		case 3: aTxBuffer[1] = 0xc3; break;
		case 4: aTxBuffer[1] = 0xb4; break;
		case 5: aTxBuffer[1] = 0xa5; break;
		case 6: aTxBuffer[1] = 0x96; break;
		case 7: aTxBuffer[1] = 0x87; break;
		default: aTxBuffer[1] = 0xf0; break;
	}
	
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 2, I2C_TIMEOUT)!= HAL_OK) 
		return 0xff;
	
	osDelay(2);			// TI! for freertos
	
	return 0;
}

uint8_t DS2482_OWI_RST(void)
{
	uint8_t res;

	aTxBuffer[0] = 0xb4; // transmiting one wire reset command
	
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 1, I2C_TIMEOUT)!= HAL_OK) 
		return 0xff;
	
	while(0!=(res=DS2482_OWI_Busy()))
	{
		if(0xff==res) return 0xff;
		osDelay (1);
	}
	
	res = DS2482_ReadStatus();
	if(res & PPD)
	{
		return 0;
	}
	else
	{
		return 2;
	}
}	

uint8_t DS2482_OWI_SINGLEBIT_WRITE(uint8_t bit)
{
	aTxBuffer[0] = 0x87; // transmiting command code (1-wire single bit sequence)
	if(bit) // transmiting command type of byte
		aTxBuffer[1] = 0x80;
	else
		aTxBuffer[1] = 0x00;
	
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 2, I2C_TIMEOUT)!= HAL_OK) 
		return 0xff;
		
	while(DS2482_OWI_Busy()) 
		osDelay (1);
	
	return 0;
}

uint8_t DS2482_OWI_SINGLEBIT_READ()
{
	uint8_t res;
	
	aTxBuffer[0] = 0x87; // transmiting command code (1-wire single bit sequence)
	aTxBuffer[1] = 0x80; // transmiting command type of byte
	
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 2, I2C_TIMEOUT)!= HAL_OK) 
		return 0xff;
	
	while(DS2482_OWI_Busy()) 
		osDelay (1);
	
	res = DS2482_ReadStatus();
	if(res & SBR)
	{ 
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t DS2482_OWI_Busy(void)
{
	uint8_t res;
	res = DS2482_ReadStatus();
	
	if(res == 0xff)
	{
		return 0xff;
	}
	
	if(res & WB)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//write triplet sequence to the selected bus
// returns:
// 2 if  bit1=0, bit2=0 (branch)
// 0 if  bit1=0, bit2=1
// 1 if  bit1=1, bit2=0
// 0xff if bit1=1, bit2=1

uint8_t DS2482_OWI_TRIPLET(uint8_t direction)
{
	
	uint8_t res;
	uint8_t bit1, bit2;
	
	
	aTxBuffer[0] = 0x78; // transmiting command code (1-wire single bit sequence)
	if(direction)  // transmiting direction
		aTxBuffer[1] =0x80;
	else
		aTxBuffer[1] =0x00;
	
	if(TI_HAL_I2C_Master_Transmit(&I2cHandle, DS2482_I2C_ADDR, (uint8_t*)aTxBuffer, 2, I2C_TIMEOUT)!= HAL_OK)  
		return 0xff;
		
	while(DS2482_OWI_Busy()) 
		osDelay(2);
	
	res = DS2482_ReadStatus();
	bit1 = res & SBR;
	bit2 = res & TSB;
	if( (0==bit1) && (0!=bit2) )
	{ 
		return 0;
	}
	if( (0!=bit1) && (0==bit2) )
	{ 
		return 1;
	}
	if( (0==bit1) && (0==bit2) )
	{ 
		return 2;
	}
	if( (0!=bit1) && (0!=bit2) )
	{ 
		return 0xff;
	}
	return 0xff;
	
}


