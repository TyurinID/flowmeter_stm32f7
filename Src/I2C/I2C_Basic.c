
#include "I2C/I2C_Basic.h"


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* I2C handler declaration */
I2C_HandleTypeDef I2cHandle;

static void TI_I2C_TransferConfig(I2C_HandleTypeDef *hi2c,  uint16_t DevAddress, uint8_t Size, uint32_t Mode, uint32_t Request);
static HAL_StatusTypeDef TI_I2C_IsAcknowledgeFailed(I2C_HandleTypeDef *hi2c);
static HAL_StatusTypeDef TI_I2C_WaitOnSTOPFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Timeout);
static HAL_StatusTypeDef TI_I2C_WaitOnFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Flag, FlagStatus Status, uint32_t Timeout);

uint8_t I2C_Ready;		

void I2C_Init(void)
{
	/*##-1- Configure the I2C peripheral ######################################*/
  I2cHandle.Instance             = I2Cx;
  I2cHandle.Init.Timing          = I2C_TIMING;
  I2cHandle.Init.OwnAddress1     = 1;
  I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
  I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  I2cHandle.Init.OwnAddress2     = 0xFF;
  I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;  
  
  if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
	{
		while(1)
		{
			__NOP();
		}
	}	
	I2C_Ready = 0;		
}

/* ================================================================================================ */
/* ============================= Standart HAL Functions Reinitialization: ========================= */
/* ================================================================================================ */
/* =================================================================TI=TyurinIvan================== */
/**
  * @brief  Transmits in master mode an amount of data in blocking mode.
  * @param  hi2c : Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  DevAddress: Target device address
  * @param  pData: Pointer to data buffer
  * @param  Size: Amount of data to be sent
  * @param  Timeout: Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef TI_HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{	
  if(hi2c->State == HAL_I2C_STATE_READY)
  {
		uint32_t tickstart = 0x00;
		tickstart = xTaskGetTickCount();
		
//		uint16_t timeout_cnt = (uint16_t)(Timeout/I2C_TRANSMIT_DELAY);
		
    /* Process Locked */
    __HAL_LOCK(hi2c);
    
    hi2c->State = HAL_I2C_STATE_MASTER_BUSY_TX;
    hi2c->ErrorCode   = HAL_I2C_ERROR_NONE;
    
    /* Send Slave Address */
    /* Set NBYTES to write */
    TI_I2C_TransferConfig(hi2c,DevAddress,Size, I2C_AUTOEND_MODE, I2C_GENERATE_START_WRITE);
		
    do			// Send byte after byte...:
    {
			// Wait until TXIS flag is set =======:
  		while(__HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_TXIS) == RESET)
			{
				if(TI_I2C_IsAcknowledgeFailed(hi2c) != HAL_OK)		// If NACK flag detected, or timeout error.
				{
					if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
					{
						return HAL_ERROR;
					}
					else
					{
						return HAL_TIMEOUT;
					}
				}
				
				/* Check for the Timeout */
				if((Timeout == 0)||((xTaskGetTickCount() - tickstart )*portTICK_PERIOD_MS > Timeout))
				{
					hi2c->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
					hi2c->State= HAL_I2C_STATE_READY;

					/* Process Unlocked */
					__HAL_UNLOCK(hi2c);

					return HAL_TIMEOUT;
				}
			}
			
      /* Write data to TXDR */
      hi2c->Instance->TXDR = (*pData++);
      Size--;	
			
    }while(Size > 0);  
		
//		osDelay(I2C_TRANSMIT_DELAY);			// TI!
		
    /* No need to Check TC flag, with AUTOEND mode the stop is automatically generated */
    /* Wait until STOPF flag is set */
    if(TI_I2C_WaitOnSTOPFlagUntilTimeout(hi2c, Timeout) != HAL_OK)
    {
      if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
      {
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }
    
    /* Clear STOP Flag */
    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_FLAG_STOPF);
  	
    /* Clear Configuration Register 2 */
    I2C_RESET_CR2(hi2c);

    hi2c->State = HAL_I2C_STATE_READY; 	  
    
    /* Process Unlocked */
    __HAL_UNLOCK(hi2c);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY; 
  }
}



/**
  * @brief  Receives in master mode an amount of data in blocking mode. 
  * @param  hi2c : Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  DevAddress: Target device address
  * @param  pData: Pointer to data buffer
  * @param  Size: Amount of data to be sent
  * @param  Timeout: Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef TI_HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  if(hi2c->State == HAL_I2C_STATE_READY)
  {    
    /* Process Locked */
    __HAL_LOCK(hi2c);
    
    hi2c->State = HAL_I2C_STATE_MASTER_BUSY_RX;
    hi2c->ErrorCode   = HAL_I2C_ERROR_NONE;
    
    /* Send Slave Address */
    /* Set NBYTES to write */
    TI_I2C_TransferConfig(hi2c,DevAddress,Size, I2C_AUTOEND_MODE, I2C_GENERATE_START_READ);
    
    do
    {
      /* Wait until RXNE flag is set */
      if(TI_I2C_WaitOnFlagUntilTimeout(hi2c, I2C_FLAG_RXNE, RESET, Timeout) != HAL_OK)      
      {
        return HAL_TIMEOUT;
      }
     
      /* Write data to RXDR */
      (*pData++) =hi2c->Instance->RXDR;
      Size--;
			
    }while(Size > 0);
    
//		osDelay(I2C_TRANSMIT_DELAY);			// TI!
		
    /* No need to Check TC flag, with AUTOEND mode the stop is automatically generated */
    /* Wait until STOPF flag is set */
    if(TI_I2C_WaitOnSTOPFlagUntilTimeout(hi2c, I2C_TIMEOUT_STOPF) != HAL_OK)
    {
      if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
      {
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }
    
    /* Clear STOP Flag */
    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_FLAG_STOPF);
  	
    /* Clear Configuration Register 2 */
    I2C_RESET_CR2(hi2c);
    
    hi2c->State = HAL_I2C_STATE_READY; 	  
    
    /* Process Unlocked */
    __HAL_UNLOCK(hi2c);
    
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY; 
  }
}

/*========================== static functions: ======================= */

/**
  * @brief  Handles I2Cx communication when starting transfer or during transfer (TC or TCR flag are set).
  * @param  hi2c: I2C handle.
  * @param  DevAddress: specifies the slave address to be programmed.
  * @param  Size: specifies the number of bytes to be programmed.
  *   This parameter must be a value between 0 and 255.
  * @param  Mode: new state of the I2C START condition generation.
  *   This parameter can be one of the following values:
  *     @arg I2C_RELOAD_MODE: Enable Reload mode .
  *     @arg I2C_AUTOEND_MODE: Enable Automatic end mode.
  *     @arg I2C_SOFTEND_MODE: Enable Software end mode.
  * @param  Request: new state of the I2C START condition generation.
  *   This parameter can be one of the following values:
  *     @arg I2C_NO_STARTSTOP: Don't Generate stop and start condition.
  *     @arg I2C_GENERATE_STOP: Generate stop condition (Size should be set to 0).
  *     @arg I2C_GENERATE_START_READ: Generate Restart for read request.
  *     @arg I2C_GENERATE_START_WRITE: Generate Restart for write request.
  * @retval None
  */
static void TI_I2C_TransferConfig(I2C_HandleTypeDef *hi2c,  uint16_t DevAddress, uint8_t Size, uint32_t Mode, uint32_t Request)
{
  uint32_t tmpreg = 0;
  
//  /* Check the parameters */
//  assert_param(IS_I2C_ALL_INSTANCE(hi2c->Instance));
//  assert_param(IS_TRANSFER_MODE(Mode));
//  assert_param(IS_TRANSFER_REQUEST(Request));
    
  /* Get the CR2 register value */
  tmpreg = hi2c->Instance->CR2;
  
  /* clear tmpreg specific bits */
  tmpreg &= (uint32_t)~((uint32_t)(I2C_CR2_SADD | I2C_CR2_NBYTES | I2C_CR2_RELOAD | I2C_CR2_AUTOEND | I2C_CR2_RD_WRN | I2C_CR2_START | I2C_CR2_STOP));
  
  /* update tmpreg */
  tmpreg |= (uint32_t)(((uint32_t)DevAddress & I2C_CR2_SADD) | (((uint32_t)Size << 16 ) & I2C_CR2_NBYTES) | \
            (uint32_t)Mode | (uint32_t)Request);
  
  /* update CR2 register */
  hi2c->Instance->CR2 = tmpreg;  
} 


/**
  * @brief  This function handles Acknowledge failed detection during an I2C Communication.
  * @param  hi2c : Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Timeout: Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef TI_I2C_IsAcknowledgeFailed(I2C_HandleTypeDef *hi2c)
{	
  if(__HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_AF) == SET)
  {
    /* Generate stop if necessary only in case of I2C peripheral in MASTER mode */
    if((hi2c->State == HAL_I2C_STATE_MASTER_BUSY_TX) || (hi2c->State == HAL_I2C_STATE_MEM_BUSY_TX)
       || (hi2c->State == HAL_I2C_STATE_MEM_BUSY_RX))
    {
      /* No need to generate the STOP condition if AUTOEND mode is enabled */
      /* Generate the STOP condition only in case of SOFTEND mode is enabled */
      if((hi2c->Instance->CR2 & I2C_AUTOEND_MODE) != I2C_AUTOEND_MODE)
      {
        /* Generate Stop */
        hi2c->Instance->CR2 |= I2C_CR2_STOP;
      }
    }		
		
    /* Wait until STOP Flag is reset */
    /* AutoEnd should be initiate after AF */
    uint16_t timeout_cnt = 0xffff;
		while(__HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_STOPF) == RESET)
    {
			timeout_cnt--;
      /* Check for the Timeout */
      if(timeout_cnt == 0)
      {
				hi2c->State= HAL_I2C_STATE_READY;
        /* Process Unlocked */
        __HAL_UNLOCK(hi2c);
        return HAL_TIMEOUT;
      }
//			osDelay(1);			// TI!
    }

    /* Clear NACKF Flag */
    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_FLAG_AF);

    /* Clear STOP Flag */
    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_FLAG_STOPF);

    /* Clear Configuration Register 2 */
    I2C_RESET_CR2(hi2c);

    hi2c->ErrorCode = HAL_I2C_ERROR_AF;
    hi2c->State = HAL_I2C_STATE_READY;

    /* Process Unlocked */
    __HAL_UNLOCK(hi2c);

    return HAL_ERROR;
  }
  return HAL_OK;
}

/**
  * @brief  This function handles I2C Communication Timeout for specific usage of STOP flag.
  * @param  hi2c : Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Timeout: Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef TI_I2C_WaitOnSTOPFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Timeout)
{  
  uint32_t tickstart = 0x00;
  tickstart = xTaskGetTickCount();
  
  while(__HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_STOPF) == RESET)
  {
    /* Check if a NACK is detected */
    if(TI_I2C_IsAcknowledgeFailed(hi2c) != HAL_OK)
    {
      return HAL_ERROR;
    }
		
    /* Check for the Timeout */
    if((Timeout == 0)||((xTaskGetTickCount() - tickstart )*portTICK_PERIOD_MS > Timeout))
    {
      hi2c->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
      hi2c->State= HAL_I2C_STATE_READY;

      /* Process Unlocked */
      __HAL_UNLOCK(hi2c);

      return HAL_TIMEOUT;
    }
//		osDelay(1);			// TI!
  }
  return HAL_OK;
}

/**
  * @brief  This function handles I2C Communication Timeout.
  * @param  hi2c : Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Flag: specifies the I2C flag to check.
  * @param  Status: The new Flag status (SET or RESET).
  * @param  Timeout: Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef TI_I2C_WaitOnFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Flag, FlagStatus Status, uint32_t Timeout)  
{  
  uint32_t tickstart = xTaskGetTickCount();
     
  /* Wait until flag is set */
  if(Status == RESET)
  {    
    while(__HAL_I2C_GET_FLAG(hi2c, Flag) == RESET)
    {
      /* Check for the Timeout */
      if(Timeout != HAL_MAX_DELAY)
      {
        if((Timeout == 0)||((xTaskGetTickCount() - tickstart )*portTICK_PERIOD_MS > Timeout))
        {
          hi2c->State= HAL_I2C_STATE_READY;
          /* Process Unlocked */
          __HAL_UNLOCK(hi2c);
          return HAL_TIMEOUT;
        }
      }
    }
  }
  else
  {
    while(__HAL_I2C_GET_FLAG(hi2c, Flag) != RESET)
    {
      /* Check for the Timeout */
      if(Timeout != HAL_MAX_DELAY)
      {
        if((Timeout == 0)||((xTaskGetTickCount() - tickstart )*portTICK_PERIOD_MS > Timeout))
        {
          hi2c->State= HAL_I2C_STATE_READY;
          /* Process Unlocked */
          __HAL_UNLOCK(hi2c);
          return HAL_TIMEOUT;
        }
      }
    }
  }
  return HAL_OK;
}

// TI1: IT disabled, polling enabled.
///******************************************************************************/
///*                 STM32F7xx Peripherals Interrupt Handlers                  */
///*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
///*  available peripheral interrupt handler's name please refer to the startup */
///*  file (startup_stm32f7xx.s).                                               */
///******************************************************************************/
///**
//  * @brief  This function handles I2C event interrupt request.
//  * @param  None
//  * @retval None
//  * @Note   This function is redefined in "main.h" and related to I2C data transmission
//  */
//void I2Cx_EV_IRQHandler(void)
//{
//  HAL_I2C_EV_IRQHandler(&I2cHandle);
//	__HAL_I2C_CLEAR_FLAG(&I2cHandle, I2C_FLAG_MASK);
//	
//}

///**
//  * @brief  This function handles I2C error interrupt request.
//  * @param  None
//  * @retval None
//  * @Note   This function is redefined in "main.h" and related to I2C error
//  */
//void I2Cx_ER_IRQHandler(void)
//{
//  HAL_I2C_ER_IRQHandler(&I2cHandle);
//	__HAL_I2C_CLEAR_FLAG(&I2cHandle, I2C_FLAG_MASK);
//}


///**
//  * @brief  Tx Transfer completed callback.
//  * @param  I2cHandle: I2C handle. 
//  * @note   This example shows a simple way to report end of IT Tx transfer, and 
//  *         you can add your own implementation. 
//  * @retval None
//  */
//void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *I2cHandle)
//{
//  I2C_Ready =1;
//}


///**
//  * @brief  Rx Transfer completed callback.
//  * @param  I2cHandle: I2C handle
//  * @note   This example shows a simple way to report end of IT Rx transfer, and 
//  *         you can add your own implementation.
//  * @retval None
//  */
//void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle)
//{
//  I2C_Ready =1;
//}


///**
//  * @brief  I2C error callbacks.
//  * @param  I2cHandle: I2C handle
//  * @note   This example shows a simple way to report transfer error, and you can
//  *         add your own implementation.
//  * @retval None
//  */
//void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *I2cHandle)
//{
//  //Error_Handler();
//	I2C_Ready =1;
//}


