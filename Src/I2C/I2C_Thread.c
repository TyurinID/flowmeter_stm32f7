
#include "I2C/I2C_Thread.h"
#include "I2C/I2C_Basic.h"

#include "Application.h"


void I2CThread(void const *argument)
{
	uint8_t current_bus = 0xff;
	
	I2C_Init();
	
	ClearDS18B20List();
	
	DPotInit();
	
	osDelay(20);
	
	// Search and Initialize sensors on bus 0:
	ClearDS18B20List();
	while(DS18B20_SENSORS_PER_BUS_0 > G_ds18b20_Count)
	{
		DS2482_RST();
		DS2482_OWI_ChannelSelect(0);
		SearchBus(ds18b20_devices, DS18B20_SENSORS_PER_BUS_0, 0);	
		sort_sensors();	
	}
	// Search and Initialize sensors on bus 1:
	while((DS18B20_SENSORS_PER_BUS_1 + DS18B20_SENSORS_PER_BUS_0) > G_ds18b20_Count)
	{
		DS2482_RST();
		DS2482_OWI_ChannelSelect(0);
		if(SearchBus(ds18b20_devices, DS18B20_SENSORS_PER_BUS_1, 1) == 0x02)
			break;							// If there aren't any sensors on bus.
		sort_sensors();	
	}
	
	for(uint8_t i=0; i<2; i++)
	{
		DS2482_OWI_ChannelSelect(i);
		
		DS18B20_StartConversion();
		osDelay(10);
	}
	
	while(1)
	{
		for(uint8_t i=0; i<G_ds18b20_Count; i++)
		{
			if(current_bus != ds18b20_devices[i].bus)
			{
				DS2482_OWI_ChannelSelect(ds18b20_devices[i].bus);
				current_bus = ds18b20_devices[i].bus;
				
				DS18B20_StartConversion();
				osDelay(10);
			}
			
			int16_t temp = DS18B20_ReadTemperatureFromRomCode(ds18b20_devices[i].id);
			
			if(0x7fff != temp )
			{
				ds18b20_devices[i].temperature = temp;
				ds18b20_devices[i].CTemperature = get_sensor_temperature(ds18b20_devices[i]);			// TI.
			}
			else
			{
				ds18b20_devices[i].Iteration_cnt++;
				if(4<ds18b20_devices[i].Iteration_cnt)
				{
					ds18b20_devices[i].Iteration_cnt = 5;
					ds18b20_devices[i].temperature = 0x7fff;
				}
			}
			
			osDelay(10);
		}
		DS18B20_StartConversion();
				
		osDelay(5);
	}
}
