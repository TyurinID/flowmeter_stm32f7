
#include "I2C/ds18b20.h"
#include <string.h>

void OWI_Write(uint8_t data)
{
	uint8_t n;
	for(n=0; n<8; n++)
	{
		if(data & 0x01)
		{
			DS2482_OWI_SINGLEBIT_WRITE(1);
		}
		else
		{
			DS2482_OWI_SINGLEBIT_WRITE(0);
		}
		data >>= 1;
	}
}

uint8_t OWI_Read(void)
{
	uint8_t data = 0, n;
	uint8_t temp;
	
	for(n=0; n<8; n++)
	{
		data >>= 1;
		
		temp = DS2482_OWI_SINGLEBIT_READ();
		if(0xff == temp)
		{
			return 0xff;
		}
		if(1 == temp)
		{
			data |= 0x80;
		}
		
	}
	
	return data;
}	

uint8_t DS18B20_StartConversion(void)
{
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0xcc);
		OWI_Write(0x44);
		
		return 0;
	}
	else
	{
		return 0xff;
	}
}

int16_t DS18B20_ReadTemperature(void)
{
	int16_t res=0x7fff;
	uint8_t n, pad[8], crc;
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0xcc);
		OWI_Write(0xbe);
		for(n=0; n<8; n++)
		{
			pad[n] = OWI_Read();
		}
		crc = OWI_Read();
		if(crc == OWI_CRC(pad, sizeof(pad)))
		{
			res = ( (int16_t)pad[0] | ((int16_t)pad[1]<<8) );
		}
	}
	return res;
}

int16_t DS18B20_ReadTemperatureFromRomCode(uint8_t *RomCode)
{
	int16_t res=0x7fff;
	uint8_t n, pad[8], crc;
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0x55);  // match rom command
		for(n=0; n<8;n++)          // ROM code
		{
			OWI_Write(RomCode[n]);
		}
		OWI_Write(0xbe);
		for(n=0; n<8; n++)
		{
			pad[n] = OWI_Read();
		}
		crc = OWI_Read();
		if(crc == OWI_CRC(pad, sizeof(pad)))
		{
			res = ( (int16_t)pad[0] | ((int16_t)pad[1]<<8) );
		}
	}
	return res;
}

uint8_t DS18B20_ReadNetAdrFromRomCode(uint8_t *RomCode)
{
	int8_t res=0xff;
	uint8_t n, pad[8], crc;
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0x55);  // match rom command
		for(n=0; n<8;n++)          // ROM code
		{
			OWI_Write(RomCode[n]);
		}
		OWI_Write(0xbe);
		for(n=0; n<8; n++)
		{
			pad[n] = OWI_Read();
		}
		crc = OWI_Read();
		if(crc == OWI_CRC(pad, sizeof(pad)))
		{
			res = pad[2];
		}
	}
	return res;
}

uint8_t DS18B20_WriteNetAddr(uint8_t Number, uint8_t NetAddr)
{
	uint8_t n, pad[8], crc;
	
	DS2482_OWI_ChannelSelect(ds18b20_devices[Number].bus);
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0x55);  // match rom command
		for(n=0; n<8;n++)          // ROM code
		{
			OWI_Write(ds18b20_devices[Number].id[n]);
		}
		OWI_Write(0xb8);   // recall eeprom
	}
	else
		return 0xff;
		
	osDelay(2);
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0x55);  // match rom command
		for(n=0; n<8;n++)          // ROM code
		{
			OWI_Write(ds18b20_devices[Number].id[n]);
		}
		OWI_Write(0xbe);   // read scratchpad
		for(n=0; n<8; n++)
		{
			pad[n] = OWI_Read();
		}
		crc = OWI_Read();
		if(crc != OWI_CRC(pad, sizeof(pad)))
		{
			return 0xff;
		}
	}
	else
		return 0xff;
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0x55);  // match rom command
		for(n=0; n<8;n++)          // ROM code
		{
			OWI_Write(ds18b20_devices[Number].id[n]);
		}
		OWI_Write(0x4e);    // write scratchpad
    OWI_Write(NetAddr);
		OWI_Write(pad[3]);
		OWI_Write(pad[4]);
	}
	else
		return 0xff;
	
	if(0==DS2482_OWI_RST())
	{
		OWI_Write(0x55);  // match rom command
		for(n=0; n<8;n++)          // ROM code
		{
			OWI_Write(ds18b20_devices[Number].id[n]);
		}
		OWI_Write(0x48);    // eeprom save
	}
	else
		return 0xff;
	
	return 0;
}

uint8_t OWI_CRC(uint8_t *data, uint8_t len)
{
	uint8_t crc = 0, n, c;
	
	while(len--)
	{
		c = *(data++);
		for(n=8; n; n--)
		{
			if( (crc ^ c) & 1)
			{
				crc ^= 0x18;
				crc >>= 1;
				crc |= 0x80;
			}
			else
			{
				crc >>= 1;
			}
			c >>=1;
		}
	}
	return crc;
}



// search functional 
OWI_device ds18b20_devices[MAX_DS18B20_DEVICES];
uint8_t G_ds18b20_Count;

void ClearDS18B20List(void)   // clear sensors list
{
	uint8_t i;
	
	G_ds18b20_Count = 0; 
	for(i=0; i<MAX_DS18B20_DEVICES; i++)
	{
		ds18b20_devices[i].NotConfigured = 0x01;
		
		ds18b20_devices[i].Iteration_cnt = 0;
		
		ds18b20_devices[i].NetAdr = 0xff;
		
		ds18b20_devices[i].temperature = 0x7fff;
		
		ds18b20_devices[i].CTemperature = 85.0;
	}
}

static uint8_t OWI_SearchRom(uint8_t * bitPattern, uint8_t lastDeviation)
{
    uint8_t currentBit = 1;
    uint8_t newDeviation = 0;
    uint8_t bitMask = 0x01;
	  uint8_t bitA, bitB;

    // Send SEARCH ROM command on the bus.
    OWI_Write(0xf0);
    
     // Walk through all 64 bits.
    while (currentBit <= 64)
    {
        // Read bit from bus twice.
        bitA = DS2482_OWI_SINGLEBIT_READ();
        bitB = DS2482_OWI_SINGLEBIT_READ();

        if (bitA && bitB)
        {
            // Both bits 1 (Error).
            newDeviation = 0xff;
            return 0xff;
        }
        else if (bitA ^ bitB)
        {
            // Bits A and B are different. All devices have the same bit here.
            // Set the bit in bitPattern to this value.
            if (bitA)
            {
                (*bitPattern) |= bitMask;
            }
            else
            {
                (*bitPattern) &= ~bitMask;
            }
        }
        else // Both bits 0
        {
            // If this is where a choice was made the last time,
            // a '1' bit is selected this time.
            if (currentBit == lastDeviation)
            {
                (*bitPattern) |= bitMask;
            }
            // For the rest of the id, '0' bits are selected when
            // discrepancies occur.
            else if (currentBit > lastDeviation)
            {
                (*bitPattern) &= ~bitMask;
                newDeviation = currentBit;
            }
            // If current bit in bit pattern = 0, then this is
            // out new deviation.
            else if ( !(*bitPattern & bitMask)) 
            {
                newDeviation = currentBit;
            }
            // IF the bit is already 1, do nothing.
            else
            {
            
            }
        }
                
        
        // Send the selected bit to the bus.
        if ((*bitPattern) & bitMask)
        {
            DS2482_OWI_SINGLEBIT_WRITE(1);
        }
        else
        {
            DS2482_OWI_SINGLEBIT_WRITE(0);
        }

        // Increment current bit.    
        currentBit++;

        // Adjust bitMask and bitPattern pointer.    
        bitMask <<= 1;
        if (!bitMask)
        {
            bitMask = 0x01;
            bitPattern++;
        }
    }
    return newDeviation;
}


uint8_t SearchBus(OWI_device *devices, uint8_t len, uint8_t bus)
{
   	uint8_t i, j;
    uint8_t presence;
    uint8_t * newID;
		uint8_t * currentID;
    uint8_t lastDeviation;
    uint8_t numDevices;
		uint8_t end_position;
    
	  numDevices = G_ds18b20_Count;
		end_position = len + G_ds18b20_Count;
	
    // Initialize all addresses as zero, on bus 0 (does not exist).
    // Do a search on the bus to discover all addresses.    
    for (i = 0; i < len; i++)
    {
        devices[G_ds18b20_Count+i].bus = bus;
        for (j = 0; j < 8; j++)
        {
            devices[G_ds18b20_Count+i].id[j] = 0x00;
        }
		devices[G_ds18b20_Count+i].NetAdr=0xff;
		devices[G_ds18b20_Count+i].temperature=0x7fff;
    }
    
		// select bus
		DS2482_OWI_ChannelSelect(bus);
    // Find the buses with slave devices.
    presence = DS2482_OWI_RST();
    if(presence != 0)
			return presence;				// If no devices on bus return 0x02!
		
    newID = devices[numDevices].id;
    

    lastDeviation = 0;
    currentID = newID;
    if (0==presence) // Devices available on this bus.
    {
       // Do slave search on each bus, and place identifiers and corresponding
       // bus "addresses" in the array.
       do  
       {
           memcpy(newID, currentID, 8);
           DS2482_OWI_RST();
           lastDeviation = OWI_SearchRom(newID, lastDeviation);
           currentID = newID;
           devices[G_ds18b20_Count].bus = bus;
           G_ds18b20_Count++;
           newID=devices[G_ds18b20_Count].id;                
       }  while(lastDeviation != 0 & MAX_DS18B20_DEVICES>G_ds18b20_Count & end_position>G_ds18b20_Count);            
    }

    // Go through all the devices and do CRC check.
    for (i = numDevices; i < G_ds18b20_Count; i++)
    {
        // If any id has a crc error, return error.
        if(OWI_CRC(devices[i].id, 7)  != devices[i].id[7])
        {
            return 0xff;
        }
		//devices[i].NetAdr=DS18B20_ReadNetAdrFromRomCode(devices[i].id);
		
		devices[i].NetAdr = FindDs18b20NetAddr(devices[i].id);
		
		devices[i].NotConfigured =0x00;
    }
    // Else, return Successful.
    return 0;
}


void sort_sensors(void)
{
	uint8_t i, j;
	OWI_device temp;
	
	for(i=0; i<G_ds18b20_Count; i++)
	{
		for(j=i; j<G_ds18b20_Count; j++)
		{
			if(ds18b20_devices[j].NetAdr<ds18b20_devices[i].NetAdr)
			{
				memcpy(temp.id, ds18b20_devices[i].id, 8);
				temp.bus = ds18b20_devices[i].bus;
				temp.NetAdr = ds18b20_devices[i].NetAdr;
				temp.NotConfigured = ds18b20_devices[i].NotConfigured;
				temp.temperature = ds18b20_devices[i].temperature;
				
				memcpy(ds18b20_devices[i].id, ds18b20_devices[j].id, 8);
				ds18b20_devices[i].bus = ds18b20_devices[j].bus;
				ds18b20_devices[i].NetAdr = ds18b20_devices[j].NetAdr;
				ds18b20_devices[i].NotConfigured = ds18b20_devices[j].NotConfigured;
				ds18b20_devices[i].temperature = ds18b20_devices[j].temperature;
				
				memcpy(ds18b20_devices[j].id, temp.id, 8);
				ds18b20_devices[j].bus = temp.bus;
				ds18b20_devices[j].NetAdr = temp.NetAdr;
				ds18b20_devices[j].NotConfigured = temp.NotConfigured;
				ds18b20_devices[j].temperature = temp.temperature;
			}
		}
	}
}

void GenerateDS18B20List(void)
{	
	ClearDS18B20List();
	SearchBus(ds18b20_devices, DS18B20_SENSORS_PER_BUS_0, 0);
	SearchBus(ds18b20_devices, DS18B20_SENSORS_PER_BUS_1, 1);
	
	sort_sensors();	
}

float get_sensor_temperature(OWI_device device)
{
	float res;
	
	res = (float)device.temperature*(float)0.0625;
	
	return res;
}

uint8_t get_sensor_not_ready(OWI_device device)
{
	if(device.NotConfigured)
	{
		return 1;
	}
	
	if(0x7fff == device.temperature)
	{
		return 1;
	}
	
	if(0x0550 <= device.temperature)
	{
		return 1;
	}
	
	return 0;
}


