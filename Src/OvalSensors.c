/**
  ************************************************************************************************
  * @file	   OvalSensors.c
  * @author  Kortev Oleg
  * @version V3.2
  * @date    2016
  * @brief   Oval data calculation functions.
  *************************************************************************************************
  */
#include "OvalSensors.h"
#include "Application.h"

TOval GOval[OVAL_SENSORS_CNR];

void OvalInit(void) // init oval sensor
{
	
	for(uint8_t i=0; i<CHANNEL_NUM; i++)
	{
		GOval[i].FPulse = 0; 
		GOval[i].FBufCnt = 0;
//		GOval[i].FBufSum = 0;
		GOval[i].FTimes_cnr = 0;
		GOval[i].FTimesBufCnt = 0;
		GOval[i].FAverageTime = 0;
		for(uint16_t j=0; j<OVAL_BUF_MAX_SIZE; j++)
		{
//			GOval[i].FBuffer[j] = 0;
			GOval[i].FTimesBuffer[j] = 0;
		}
		GOval[i].FCalibration = 3000; // todo initialize in flash registers. TI: DELETE?
		
		GOval[i].FReady = 1;
		
		GOval[i].FMeasureLatch = 0;
		
		GOval[i].FFirstCycle = 1;
		
		GOval[i].FMeasurementTime = 64;	// TI: initialized in flash map! DELETE?
	}
}

#define OVAL_LOG_PERIOD_MS 2500

void UpdateOval(TOval *oval)
{
	uint16_t CyclBuffEnd;				// TI: CyclicBufferEnd.
	if(oval->FFirstCycle)				// TI: CyclicBufferEnd value selection . 	//&OVAL_BUF_END;
	{
		CyclBuffEnd = (oval->FBufCnt+1);  // TI: At first cycle .
	}
	else
	{
		CyclBuffEnd = GOvalBuffSize; // TI: Second cycles and following.
	}
	
	// Time handling:
	uint64_t bufsum_temp =0;			// TI: buffer sum.
	uint16_t valid_times =0;			// TI: times other than zero.
	oval->FTimesBuffer[oval->FBufCnt] = oval->FTimes_cnr;		// TI: put new value to CyclBuffer.
	
	for(uint16_t cycle_cnr1 = 0; cycle_cnr1 < CyclBuffEnd; cycle_cnr1++)
	{
		if(0!= oval->FTimesBuffer[cycle_cnr1])
		{
			bufsum_temp += oval->FTimesBuffer[cycle_cnr1];
			valid_times++;
		}
	}
	
	if(0<valid_times)
			oval->FAverageTime = (float)bufsum_temp/(float)valid_times;			// TI: calculate average time.
	else
			oval->FAverageTime = OVAL_MAX_PERIOD;
		
	oval->FBufCnt++;
	if(oval->FBufCnt >= GOvalBuffSize)		// TI: if Cyclic Buffer full, go to beginning and FirstCycle is done.
	{
		oval->FBufCnt = 0;
		if(oval->FFirstCycle == 1)
			oval->FFirstCycle = 0;					// TI: MB + if(FirstCycle).
	}	
}

float CountOvalOutput(TOval *oval, uint8_t unit_id, float taho) // count output value
{
	// todo place right oval calculation here
	
	float average = 0;
	if(0 != oval->FAverageTime)
	{
		average = ((float)10000)/((float)(oval->FAverageTime));
		
		if(OVAL_MAX_PERIOD<=oval->FAverageTime)
			average = 0;
	}
	
	float res;
	
	switch(unit_id)
	{
		case 1: // ml/100_str
			if( (float)0.0 != taho)
				res = 60000*100*average/((float)(oval->FCalibration)*taho);
			else
				res = 0.0;
			break;
		case 2: // ml/200_str
			if( (float)0.0 != taho)
				res = 60000*200*average/((float)(oval->FCalibration)*taho);
			else
				res = (float)0.0;
			break;
		case 3: // ml/1000_str
			if( (float)0.0 != taho)
				res = 60000*1000*average/((float)(oval->FCalibration)*taho);
			else
				res= (float)0.0;
			break;
		case 4: // l/h
			res = 3600*average/((float)(oval->FCalibration));
			break;
		
		case 0:  // ml/min
		default:
			res = 60000*average/((float)(oval->FCalibration));
			break;
	}
	
	return res;
}

void ResetOval(TOval *oval)  // reset oval
{
	for(uint16_t j=0; j<OVAL_BUF_MAX_SIZE; j++)
	{
			oval->FTimesBuffer[j] = 0;
	}
//	oval->FBufSum = 0;
	oval->FAverageTime = 0;
	oval->FBufCnt = 0;
	
	oval->FPulse = 0;
	
	oval->FFirstCycle = 1;
}


uint8_t OvalSensorReady(TOval *oval)
{
	return 1;
}

