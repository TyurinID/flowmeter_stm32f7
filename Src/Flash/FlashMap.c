
#include "FLash/FlashMap.h"


TFlashSavingRegister GFlashSavingRegister[FLASHMAP_COUNT]; // flash data map


/* Private variables ---------------------------------------------------------*/
uint32_t FirstSector = 0, NbOfSectors = 0;
uint32_t Address = 0, SECTORError = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

/*Variable used for Erase procedure*/

/**
  * @brief  Gets the sector of a given address
  * @param  None
  * @retval The sector of a given address
  */


uint8_t SaveNewFlashMap(uint16_t size)
{
	taskENTER_CRITICAL();
	// erase
  HAL_FLASH_Unlock();

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/


  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
     you have to make sure that these data are rewritten before they are accessed during code
     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
     DCRST and ICRST bits in the FLASH_CR register. */
  //if (HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError) != HAL_OK)
  //{
		//return 0xff;
	//}
	
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_SR_ERSERR | FLASH_SR_BSY);

	FLASH_Erase_Sector(FLASH_SECTOR_7, VOLTAGE_RANGE_3);
	
	// program
	//Address = FLASH_USER_START_ADDR;
	
	uint16_t act_size = (size>FLASHMAP_COUNT)? FLASHMAP_COUNT : size; // choose minimum
	
  for(uint16_t i = 0; i<act_size ; i++)
  {
		Address = FLASH_USER_START_ADDR + i*4;
		uint64_t data_in = (uint64_t)(*(GFlashSavingRegister[i].FTarget));
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, data_in ) != HAL_OK)
		{
      //return 0xff;
    }
  }
	
  HAL_FLASH_Lock();
	
	taskEXIT_CRITICAL();
	
	// veryfy
	Address = FLASH_USER_START_ADDR;
  MemoryProgramStatus = 0x0;

  for(uint16_t i = 0; i<act_size ; i++)
  {
		Address = FLASH_USER_START_ADDR + i*4;
		
    data32 = *(__IO uint32_t *)Address;
		
    if (data32 != *(GFlashSavingRegister[i].FTarget))
    {
      MemoryProgramStatus++;
    }
    
  }
	if(0!=MemoryProgramStatus)
		return 0xff;
	return 0;
}

void RecallFlashMap(uint16_t size)
{
	//Address = FLASH_USER_START_ADDR;
	
	uint16_t act_size = (size>FLASHMAP_COUNT)? FLASHMAP_COUNT : size; // choose minimum
	
	for(uint16_t i = 0; i<act_size ; i++)
  {
		Address = FLASH_USER_START_ADDR + i*4;
		
    *(GFlashSavingRegister[i].FTarget) = *( (__IO uint32_t *)(Address) );
		GFlashSavingRegister[i].FBufferValue = *(GFlashSavingRegister[i].FTarget);
	}
}


uint8_t CheckFlashMap(uint16_t size)
{
	Address = FLASH_USER_START_ADDR;
	
	uint16_t act_size = (size>FLASHMAP_COUNT)? FLASHMAP_COUNT : size; // choose minimum
	
	for(uint16_t i = 0; i<act_size ; i++)
  {
    //*(GFlashSavingRegister[i].FTarget) = *( (__IO uint32_t *)(Address) );
		//GFlashSavingRegister[i].FBufferValue = *(GFlashSavingRegister[i].FTarget);
		
		if(*(GFlashSavingRegister[i].FTarget) != GFlashSavingRegister[i].FBufferValue)
			return 0;
		
	}
	
	return 1;
}


