/**
  ************************************************************************************************
  * @file	   SettingsWindow.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    16-December-2016
  * @brief   File with functions for Main Window drawing and process.
	*					 Settings works in reaction mode. Something changes only if display is touched.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:
#include "GraphicTask/SettingsWindow.h"

#include <string.h>
#include "Application.h"
#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/MainWindow.h"
#include "GraphicTask/GraphKeyBoard.h"
#include "GraphicTask/GeneralPage.h"
#include "GraphicTask/CnannelPage.h"
#include "GraphicTask/NetworkPage.h"
#include "GraphicTask/ThermometersWindow.h"
#include "GraphicTask/LogPage.h"
#include "FLash/FlashMap.h"

// --------------------------------------------------------- Global variables --------------------:
uint8_t GToDrawFLG;			// FLG to redraw window (Settings or Thermometers).

// --------------------------------------------------------- Private variables -------------------:
static uint8_t page_number;				// Active page number.	
static uint8_t thermo_sett_counter = 0;			// Counter to 20 clicks for entering thermo sensors settings window.
static uint8_t logs_sett_counter	= 0;			// Counter to 10 clicks for entering logs windows.
const char settingsWinName[CHANNEL_NUM + 2][12];

// --------------------------------------------------------- Private function prototypes ---------:
static void SettingsWin_Draw(void);




// --------------------------------------------------------- Global functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @SettingsWin_Process
  *	&		@brief  Draw all graphics and react on button pressing.
  * & 	@param  TouchParameters: is touched previously, ts.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void SettingsWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts)
{
	// Redraw everything if something changed ==========================:
	if(GToDrawFLG)
	{
		GToDrawFLG = 0;
		thermo_sett_counter = 0;		
		logs_sett_counter = 0;
		if(GKeyBoard.Active)
			KeyBoard_Draw();			// Draw Keyboard if it's active.
		else
			SettingsWin_Draw();		// Draw buttons, labels and Page(page_number).
	}
	
	// Execute buttons pressing ========================================:
	if(ts.touchDetected == 1 && touched_prev == 0)
	{	
		vTaskSuspendAll ();
		if( BUTTON_Y0 < ts.touchY[0] )		// Buttons:
		{
			// Button "<" ==========================================:
			if( ts.touchX[0] < BUTTON_LEFT_X1 )
			{
				if(page_number>0 && (page_number != CHANNEL_NUM/2 + 2))
				{
					page_number--;
					GToDrawFLG = 1;
				}
				else				// Transition between Logs and Normal Sett:
				{
					if(++logs_sett_counter == 5)	
					{
						page_number--;
						GToDrawFLG = 1;
					}
				}
			}
			// Button ">" ==========================================:
			if( ts.touchX[0] > BUTTON_RIGHT_X0 )
			{
				if( (page_number<CHANNEL_NUM + 1) && (page_number != CHANNEL_NUM/2 + 1) )
				{
					logs_sett_counter = 0;
					page_number++;
					GToDrawFLG = 1;
				}
				else				// Transition between Normal Sett and Logs:
				{
					if(++logs_sett_counter == 10)	
					{
						page_number++;
						GToDrawFLG = 1;
					}
				}
			}
				// Only for developers ==============================:
			if ( BUTTON_LEFT_X1 < ts.touchX[0] && ts.touchX[0] < BUTTON_RIGHT_X0 ) 
			{
				if(++thermo_sett_counter == 20)
				{
					GActiveWindow = THERMOMETERS_WIN;		// Go to Thermometers Settings.
					thermo_sett_counter = 0;
				}
			}
		}
		else
		{
			// "STARDEX" pushed ====================================:
			if(ts.touchX[0] < 45)				
			{
				if(0==CheckFlashMap(GFlashMapSize))
				{
					SaveNewFlashMap(GFlashMapSize);
					RecallFlashMap(GFlashMapSize);
				}				
				thermo_sett_counter = 0;
				logs_sett_counter	= 0;
				GKeyBoard.Active = 0;
				GDrawButtonsAndLabels = DRAW_ALL;
				GActiveWindow = MAIN_WIN;
			}	
			else
			// Something on page pressed ===========================:
			{
				if(GKeyBoard.Active)
					KeyBoard_Process(ts);
				else
					switch(page_number)
					{
						case 0:
							GeneralPage_Process(ts);
							break;
						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
							ChannelPage_Process(ts,page_number);
							break;
						case 6:
							NetworkPage_Process(ts);
							break;
						case 7:
						case 8:
						case 9:
						case 10:
						case 11:
							LogPage_Process(ts);
							break;
					}
			}
		}	
		xTaskResumeAll ();
	}
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @SettingsWin_Init
  *	&		@brief  Initialize parameters before first execution of SettingsWin_Process().
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void SettingsWin_Init(void)
{
	logs_sett_counter	= 0;
	thermo_sett_counter = 0;
	page_number = 0;
	GToDrawFLG = 0;
}





// -------------------------------------------------------- Private functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @SettingsWin_Draw
  *	&		@brief  Draw buttons, names and call page_draw function.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void SettingsWin_Draw(void)
{
	GUI_RECT Rect;
	GUI_SetTextMode(GUI_TM_TRANS);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_SetFont(GUI_FONT_24_ASCII);
	
	// Buttons and window_name =========:
	GUI_HMEM hMem = GUI_MEMDEV_Create(GScreen_sector[5].FXBegin, 
																		 GScreen_sector[5].FYBegin, 
																		 GScreen_sector[5].FXEnd-GScreen_sector[5].FXBegin, 
																		 GScreen_sector[5].FYEnd-GScreen_sector[5].FYBegin);
	GUI_MEMDEV_Select(hMem);
	GUI_Clear();

	GUI_DrawBitmap(&bmButtonYellow, BUTTON_LEFT_X0, BUTTON_Y0);
	Rect.y0 = BUTTON_Y0;
	Rect.y1 = BUTTON_Y1;
	Rect.x0 = BUTTON_LEFT_X0;
	Rect.x1 = BUTTON_LEFT_X1;
	GUI_DispStringInRectWrap("<", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);

	GUI_DrawBitmap(&bmButtonYellow, BUTTON_RIGHT_X0, BUTTON_Y0);
	Rect.x0 = BUTTON_RIGHT_X0;
	Rect.x1 = BUTTON_RIGHT_X1;
	GUI_DispStringInRectWrap(">", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
																
	Rect.x0 = BUTTON_LEFT_X1+5;
	Rect.x1 = BUTTON_RIGHT_X0-5;
	GUI_SetColor(FM_COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_DispStringInRectWrap(settingsWinName[page_number], &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	Rect.y0 = BUTTON_Y0+1;
	Rect.y1 = BUTTON_Y1-1;
	Rect.x0 = BUTTON_LEFT_X1+6;
	Rect.x1 = BUTTON_RIGHT_X0-6;
	GUI_DrawRectEx(&Rect);	
	
	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);
	
	// Page field =====================:
	for(uint8_t i=0; i<5; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYBegin, 
														 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();		
	
		switch(page_number)
		{
			case 0:
				GeneralPage_Draw();
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				ChannelPage_Draw(page_number);
				break;
			case 6:
				NetworkPage_Draw();
				break;
			case 7:
				LogPage_Draw(&GChannel[0],&GChannel[4]);
				break;
			case 8:
				LogPage_Draw(&GChannel[1],&GChannel[5]);
				break;
			case 9:
				LogPage_Draw(&GChannel[2],&GChannel[6]);
				break;
			case 10:
				LogPage_Draw(&GChannel[3],&GChannel[7]);
				break;
			case 11:
				LogPage_Draw(&GChannel[8],&GChannel[9]);
				break;
		}
				
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}

// --------------------------------------------------------- Private constants -------------------:
const char settingsWinName[CHANNEL_NUM + 2][12] = {
																"General",
																"Channel 1",
																"Channel 2",
																"Channel 3",
																"Channel 4",
																"Pump",
																"Network",
																"Ch1 Logs",
																"Ch2 Logs",
																"Ch3 Logs",
																"Ch4 Logs",
																"Pump Logs"};
