/**
  ************************************************************************************************
  * @file	   LogPage.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Display logs tables and react on screen touching.
  *************************************************************************************************
  */
	
#include "GraphicTask/LogPage.h"

#include "GraphicTask/MainWindow.h"
#include "GraphicTask/SettingsWindow.h"


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @LogPage_Process
  *	&		@brief  React on button pressing.
  * & 	@param  TouchParameter: ts.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void LogPage_Process(__IO TS_StateTypeDef  ts)
{

}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @LogPage_Draw
  *	&		@brief  Draw logs tabels. Two tables per channel. Two channels per page.
	* & 	@param  *first_ch: pointer to first channel to be logged on this page;
	* &						*second_ch: pointer to second channel to be logged on this page;
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void LogPage_Draw(TChannel* first_ch, TChannel* second_ch)
{	
	GUI_RECT Rect;
	GUI_SetFont(GUI_FONT_24_ASCII);
	GUI_SetColor(FM_COLOR_BK2);
	Rect.y0 = LABEL_Y0;
	Rect.y1 = LABEL_Y1;
	Rect.x0 = LABEL1_X0;
	Rect.x1 = LABEL1_X1;
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_DispStringInRectWrap("DELIVERY", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
			
	GUI_SetColor(FM_COLOR_BK2);
	Rect.x0 = LABEL2_X0;
	Rect.x1 = LABEL2_X1;
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_DispStringInRectWrap("BACK FLOW", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	
	GUI_SetFont(&GUI_Font16_1);
	GUI_SetTextMode(GUI_TM_TRANS);
		
	for(uint8_t i=0; i<4; i++)
	{
		TChannel* curr_channel;
		
		if(i<2)
			curr_channel = first_ch;
		else
			curr_channel = second_ch;
		
		// draw BIG fields:
		Rect.x0 = LABEL1_X0 + i*2*FLASKS_SHIFT;
		Rect.x1 = LABEL1_X0 + i*2*FLASKS_SHIFT + 2*FLASKS_SHIFT;
		Rect.y0 = 25;
		Rect.y1 = BUTTON_Y0-5;
		GUI_SetColor(FM_COLOR_GREY);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(FM_COLOR_DARKBLUE);
		GUI_DrawRectEx(&Rect);
		Rect.x0 = LABEL1_X0 + i*2*FLASKS_SHIFT + 3;
		for(uint8_t j=0; j<LOG_LINES_NUM/2; j++)
		{
			Rect.y0 = 25+j*16;
			Rect.y1 = 25+(j+1)*16;
			uint8_t line_cnt_in_buff = curr_channel->Log.curr_line + (LOG_LINES_BUFF_SIZE - LOG_LINES_NUM) + j+(i%2)*(LOG_LINES_NUM/2) + 1;
			if(line_cnt_in_buff >= LOG_LINES_BUFF_SIZE)
				line_cnt_in_buff -= LOG_LINES_BUFF_SIZE;
			GUI_DispStringInRectWrap(curr_channel->Log.line[line_cnt_in_buff], &Rect, GUI_TA_TOP |
																	GUI_TA_LEFT, GUI_WRAPMODE_WORD);
		}
	}	
	GUI_SetFont(&GUI_Font20_1);
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @AddLog
  *	&		@brief  Add log string in log table for displaing on LogPage.
	* & 					Use following, for example: "254.2s  data=25.5".
	* & 	@param  *channel: pointer to channel to be logged;
	* &						*text: pointer to text of log.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void AddLog(TChannel* channel, char* text)
{
	if(++channel->Log.curr_line >= LOG_LINES_BUFF_SIZE)
		channel->Log.curr_line = 0;
	strcpy(channel->Log.line[channel->Log.curr_line], text);		// Write new log string.
	GToDrawFLG = 1;						// Redraw logs menu.
}


