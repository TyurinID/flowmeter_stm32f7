/**
  ************************************************************************************************
  * @file	   ThermometersWindow.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    16-December-2016
  * @brief   File with functions for Thermometers Window drawing and process.
	*					 
  *************************************************************************************************
  */
	
#include "GraphicTask/ThermometersWindow.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/GraphKeyboard.h"
#include "GraphicTask/SettingsWindow.h"
#include "GraphicTask/ScreenSectors.h"

#include "I2C/Ds18b20.h"
#include "I2C/ds18b20hash.h"


TThermoField	ThermoField[20];			// Fields in table for thermo sensors ds18b20.

static void ThermometersWin_Draw(void);
static void	Table_Draw(void);
static void	DataInTable_Draw(void);


// --------------------------------------------------------- Global functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ThermometersWin_Process
  *	&		@brief  Draw all graphics and react on button pressing.
  * & 	@param  TouchParameters: is touched previously, ts.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void ThermometersWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts)
{		
	if(GKeyBoard.Active == 0)
		ThermometersWin_Draw();
	else
		if(GToDrawFLG)
		{	
			GToDrawFLG = 0;
			KeyBoard_Draw();	
		}
	
	//handle touch screen
	if(ts.touchDetected == 1 && touched_prev == 0)
	{
		if(ts.touchX[0] < 45)																					// "STARDEX" pushed.
		{
			GActiveWindow = SETTINGS_WIN;
			GToDrawFLG = 1;
		}
		if(GKeyBoard.Active)
			KeyBoard_Process(ts);
		else
		{		
			for(uint8_t i=0; i<G_ds18b20_Count; i++)									// Thermo NetAddr Knobs:
			{
				if( ThermoField[i].x0 < ts.touchX[0] && ts.touchX[0] < ThermoField[i].x1 
						&& ThermoField[i].y0 < ts.touchY[0] && ts.touchY[0] < ThermoField[i].y1)
				{				
					GKeyBoard.Mode = KB_DRAW_ADDIT;
					GKeyBoard.Target = (uint32_t*)(&(ds18b20_devices[i].NetAdr));
					GKeyBoard.AdditTarget = &(ds18b20_devices[i].CTemperature);
					GKeyBoard.FMin = 1;
					GKeyBoard.FMax = G_ds18b20_Count+1;
					GKeyBoard.Active = 1;
					GToDrawFLG = 1;					
					return;
				}
			}
																																				// Knob "SAVE":
			if( TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) < ts.touchX[0] && ts.touchX[0] < TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1 + WIDTH_2 
						&& TABLE_Y0 + 3*HEIGHT < ts.touchY[0] && ts.touchY[0] < TABLE_Y0 + 4*HEIGHT + (uint16_t)(HEIGHT/3))
			{		
				SaveAllDs18b20();		// Save new NetAddresses in flash.
				sort_sensors();
				GToDrawFLG = 1;	
			}
			
																																				// Knob "CLEAR":
			if( TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) < ts.touchX[0] && ts.touchX[0] < TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1 + WIDTH_2 
						&& TABLE_Y0 + 5*HEIGHT - (uint16_t)(HEIGHT/3) < ts.touchY[0] && ts.touchY[0] < TABLE_Y0 + 6*HEIGHT)
			{
				for(uint8_t position=0; position<G_ds18b20_Count; position++)		// Reset NetAddresses.
					ds18b20_devices[position].NetAdr = 0xFF;
				sort_sensors();
			}
		}
	}	
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ThermometersWin_Init
  *	&		@brief  Initialize all parameters.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void ThermometersWin_Init(void) 
{	
	uint8_t counter;
	
	for(uint8_t i=0; i<3; i++)				// Initialize table coordinate.
	{
		for(uint8_t j=0; j<6; j++)
		{
			counter = i*6+j;
			ThermoField[counter].x0 = TABLE_X0 + i*(WIDTH_1 + WIDTH_2 + 10);
			ThermoField[counter].x1 = TABLE_X0 + i*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1;
			ThermoField[counter].x2 = TABLE_X0 + i*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1 + WIDTH_2;
			
			ThermoField[counter].y0 = TABLE_Y0 + j*HEIGHT;
			ThermoField[counter].y1 = TABLE_Y0 + (j+1)*HEIGHT;		
		}
	}
	for(uint8_t j=0; j<2; j++)
	{
		counter = 3*6+j;
		ThermoField[counter].x0 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10);
		ThermoField[counter].x1 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1;
		ThermoField[counter].x2 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1 + WIDTH_2;
		
		ThermoField[counter].y0 = TABLE_Y0 + j*HEIGHT;
		ThermoField[counter].y1 = TABLE_Y0 + (j+1)*HEIGHT;		
	}
}



// -------------------------------------------------------- Private functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ThermometersWin_Draw
  *	&		@brief  Draw buttons, table.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void ThermometersWin_Draw(void)
{	
	GUI_HMEM hMem = GUI_MEMDEV_Create(GScreen_sector[5].FXBegin, 
																		 GScreen_sector[5].FYBegin, 
																		 GScreen_sector[5].FXEnd-GScreen_sector[5].FXBegin, 
																		 GScreen_sector[5].FYEnd-GScreen_sector[5].FYBegin);
	GUI_MEMDEV_Select(hMem);
	GUI_Clear();

	GUI_SetFont(&GUI_Font24_1);
	GUI_SetColor(FM_COLOR_WHITE);
	GUI_DispStringAt("Only for developers! To exit click \"Stardex\"", 30, 240);

	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);
	
	for(uint8_t i=0; i<5; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
													 GScreen_sector[i].FYBegin, 
													 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin, 
													 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();
		
		Table_Draw();		
		DataInTable_Draw();	
	
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @Table_Draw
  *	&		@brief  Draw table of thermometers.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void	Table_Draw(void)
{
	GUI_RECT Rect = {TABLE_X0, TABLE_Y0, 480 , 270};
	
	for(uint8_t i=0; i<G_ds18b20_Count; i++)									// Thermo NetAddr Knobs:
	{
		Rect.x0 = ThermoField[i].x0;
		Rect.x1 = ThermoField[i].x1;
		Rect.y0 = ThermoField[i].y0;
		Rect.y1 = ThermoField[i].y1;
		GUI_SetColor(GUI_YELLOW);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(FM_COLOR_DARKBLUE);
		GUI_DrawRectEx(&Rect);
		Rect.x0 = ThermoField[i].x1;
		Rect.x1 = ThermoField[i].x2;
		GUI_SetColor(FM_COLOR_WHITE);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(FM_COLOR_DARKBLUE);
		GUI_DrawRectEx(&Rect);
	}
	
	// Knob "SAVE":
	Rect.x0 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10);
	Rect.x1 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1 + WIDTH_2;
	Rect.y0 = TABLE_Y0 + 3*HEIGHT;
	Rect.y1 = TABLE_Y0 + 4*HEIGHT + (uint16_t)(HEIGHT/3);
	GUI_SetColor(GUI_GREEN);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	GUI_SetTextMode(GUI_TM_TRANS);
	GUI_DispStringInRectWrap("SAVE", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	
	// Knob "CLEAR":
	Rect.x0 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10);
	Rect.x1 = TABLE_X0 + 3*(WIDTH_1 + WIDTH_2 + 10) + WIDTH_1 + WIDTH_2;
	Rect.y0 = TABLE_Y0 + 5*HEIGHT - (uint16_t)(HEIGHT/3);
	Rect.y1 = TABLE_Y0 + 6*HEIGHT;
	GUI_SetColor(GUI_RED);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	GUI_DispStringInRectWrap("CLEAR", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @DataInTable_Draw
  *	&		@brief  Draw temperature and device number in table.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void	DataInTable_Draw(void)
{
	// Data in Table:
	char str[]="";
	GUI_SetFont(&GUI_Font24_1);
	GUI_RECT Rect = {TABLE_X0, TABLE_Y0, 480 , 270};
	for(uint8_t i=0; i<G_ds18b20_Count; i++)									// Thermo NetAddr Knobs:
	{
		Rect.x0 = ThermoField[i].x0;
		Rect.x1 = ThermoField[i].x1;
		Rect.y0 = ThermoField[i].y0;
		Rect.y1 = ThermoField[i].y1;
		GUI_SetColor(FM_COLOR_DARKBLUE);
		sprintf (str, "%d", ds18b20_devices[i].NetAdr); 
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		
		Rect.x0 = ThermoField[i].x1;
		Rect.x1 = ThermoField[i].x2;
		sprintf (str, "%.1f", ds18b20_devices[i].CTemperature); 		
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	}
}


