/**
  ************************************************************************************************
  * @file	   MainWindow.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    16-December-2016
  * @brief   File with functions for Main Window drawing and process.
  *************************************************************************************************
  */
// -------------------------------------------------------- Includes -----------------------------:	
#include "GraphicTask/MainWindow.h"
#include "GraphicTask/SettingsWindow.h"
#include "GraphicTask/LogPage.h"

// --------------------------------------------------------- Global variables --------------------:
const uint32_t GradientOrange[FLASK_WIDTH-1];				// For orange gradient.

TDrawButtonsAndLabels 	GDrawButtonsAndLabels;			// The button sector to be drawn.

// --------------------------------------------------------- Private variables -------------------:

static TWindowPage		PumpPage;			// Pump page contains pump flasks 


// --------------------------------------------------------- Private function prototypes ---------:									
static void	PumpPage_Draw(void);
static void InjPage_Draw(void);
static void DrawButtonsAndLabels(void);
static void DrawFlask(TChannel channel, TFlaskMode mode, uint16_t x, uint16_t y, TWidth width_coeff, uint8_t flaskHeight);
static void DrawLabelsAndTaho(void);		// Draw labels "Delivery" & "Back Flow", taho field and "RPM".
static void ToggleUnits(void);



// --------------------------------------------------------- Global functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @MainWin_Process
  *	&		@brief  Draw all graphics and react on button pressing.
  * & 	@param  TouchParameters: is touched previously, ts.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void MainWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts)
{	
	// Draw graphics ======================================:
	if(GDrawButtonsAndLabels != DO_NOT_DRAW)
		DrawButtonsAndLabels();								// Draw buttons, labels and taho.
		
	if(PumpPage.Active)
		PumpPage_Draw();		// Pump flasks.
	else
		InjPage_Draw();			// Injectors flasks.
	
	if(ts.touchDetected == 1 && touched_prev == 0)
	{
		vTaskSuspendAll ();
		if( BUTTON_Y0 < ts.touchY[0] )		// Buttons:
		{
			// Button "Start/Stop" ===========================================:
			if( BUTTON3_X0 < ts.touchX[0] )
			{				
				if(GMeasurementsON == 0)
				{
					GMeasurementsON = 1;
					GDrawButtonsAndLabels = DRAW_START;
					for(uint8_t i=0; i<CHANNEL_NUM; i++)
					{
						char str[32] = "";
						sprintf(str,"%.1fs    START", (float)GDeviceWorkingTime/100 );
						AddLog(&GChannel[i], str);
						GChannel[i].FMeasureStartTimer = 0;
						ResetOutputValues(&GChannel[i]);
						ResetOval(GChannel[i].FOval);
					}			
				}
				else
				{
					GMeasurementsON = 0;
					GDrawButtonsAndLabels = DRAW_START;
				}		
			}
			
			// Button "Units" =================================================:
			if( BUTTON2_X0 < ts.touchX[0] && ts.touchX[0] < BUTTON2_X1 )	
			{	
				ToggleUnits();
				GDrawButtonsAndLabels = DRAW_UNITS;	
				SetOutputMaximums();
			}
			
			// Button "Pump" ==================================================:
			if( BUTTON1_X0 < ts.touchX[0] && ts.touchX[0] < BUTTON1_X1 )	
			{	
				if(PumpPage.Active == 0)
					PumpPage.Active = 1;
				else
					PumpPage.Active = 0;
				GDrawButtonsAndLabels = DRAW_PUMP;
			}
		}
		else
		{	// "STARDEX" pushed ==============================================:
			if(ts.touchX[0] < 45)				
			{
				GActiveWindow = SETTINGS_WIN;
				GToDrawFLG = 1;
			}				
		}
		xTaskResumeAll ();
	}
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @MainWin_Init
  *	&		@brief  Initialize parameters before first execution of MainWin_Process().
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void MainWin_Init(void)
{
	GDrawButtonsAndLabels = DRAW_ALL;
	GMeasurementsON = 0;
	GUI_SetTextMode(GUI_TM_TRANS);
}





// -------------------------------------------------------- Private functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @InjPage_Draw
  *	&		@brief  Draw injector flasks with temperature values...
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void InjPage_Draw(void)
{	
	static uint8_t sectorNum = 0;		// Draw only 1 sector each iteration.
	if(++sectorNum == 4) 						// Draw taho value once each iteration.
	{
		sectorNum =0;
		GUI_SetColor(FM_COLOR_WHITE);
		GUI_RECT Rect = {TAHO_X0+1,TAHO_Y0+1,TAHO_X1-1,TAHO_Y1-1};
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_SetFont(GUI_FONT_20_ASCII);		
		uint16_t taho_output;
		char str[5] = "";
		if(GTaho.FOutput<10000)
			taho_output = ceilf(GTaho.FOutput);
		else
			taho_output= 10000;
		sprintf (str, "%d", taho_output);
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	}
	for(uint8_t i=0; i<4; i++)
	{
		GUI_HMEM hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
																			 GScreen_sector[i].FYBegin, 
																			 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin, 
																			 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();
		
		if(G_ds18b20_Count > CHANNEL_NUM)		// for 18 thermometers
		{
				DrawFlask(GChannel[i*2], FLASK_2TH, LABEL1_X0 + i*2*FLASKS_SHIFT, 200, NORMAL, FLASK_HEIGHT);
				DrawFlask(GChannel[i*2+1], FLASK_2TH, LABEL1_X0 + i*2*FLASKS_SHIFT+FLASKS_SHIFT, 200, NORMAL, FLASK_HEIGHT);
		}
		else										// for 10 thermometers
		{
				DrawFlask(GChannel[i*2], FLASK_1TH, LABEL1_X0 + i*2*FLASKS_SHIFT, 220, NORMAL, FLASK_HEIGHT+20);
				DrawFlask(GChannel[i*2+1], FLASK_1TH, LABEL1_X0 + i*2*FLASKS_SHIFT+FLASKS_SHIFT, 220, NORMAL, FLASK_HEIGHT+20);
		}		
				
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @PumpPage_Draw
  *	&		@brief  Draw pump flasks with temperature values...
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void	PumpPage_Draw(void)
{
	static uint8_t sectorNum = 0;		// Draw only 1 sector each iteration.
	if(++sectorNum == 4) 						// Draw taho value once each iteration.
	{
		sectorNum =0;
		GUI_SetColor(FM_COLOR_WHITE);
		GUI_RECT Rect = {TAHO_X0+1,TAHO_Y0+1,TAHO_X1-1,TAHO_Y1-1};
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_SetFont(GUI_FONT_20_ASCII);		
		uint16_t taho_output;
		char str[5] = "";
		if(GTaho.FOutput<10000)
			taho_output = ceilf(GTaho.FOutput);
		else
			taho_output= 10000;
		sprintf (str, "%d", taho_output);
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	}
	
	GUI_HMEM hMem = GUI_MEMDEV_Create(GScreen_sector[sectorNum ].FXBegin, 
																		 GScreen_sector[sectorNum ].FYBegin, 
																		 GScreen_sector[sectorNum ].FXEnd-GScreen_sector[sectorNum ].FXBegin, 
																		 GScreen_sector[sectorNum ].FYEnd-GScreen_sector[sectorNum ].FYBegin);
	GUI_MEMDEV_Select(hMem);
	GUI_Clear();
	
	if(sectorNum < 2)
		DrawFlask(GChannel[8], FLASK_1TH, LABEL1_X0 + FLASKS_SHIFT, 220, DOUBLE, FLASK_HEIGHT+20);
	else
		DrawFlask(GChannel[9], FLASK_1TH, LABEL2_X0 + FLASKS_SHIFT, 220, DOUBLE, FLASK_HEIGHT+20);
			
	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @DrawButtonsAndLabels
  *	&		@brief  Draw buttons, labels and taho.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void DrawButtonsAndLabels(void)
{	
	GUI_HMEM hMem;
	GUI_RECT Rect;
	Rect.y0 = BUTTON_Y0;
	Rect.y1 = BUTTON_Y1;
	GUI_SetTextMode(GUI_TM_TRANS);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_SetFont(GUI_FONT_24_ASCII);
	
	switch(GDrawButtonsAndLabels)
	{
		case DRAW_LABELS_AND_TAHO:
			DrawLabelsAndTaho();
			break;
		
		case DRAW_PUMP:
			GUI_DrawBitmap(&bmButtonYellow, BUTTON1_X0, BUTTON_Y0);
			Rect.x0 = BUTTON1_X0;
			Rect.x1 = BUTTON1_X1;
			if(PumpPage.Active)
				GUI_DispStringInRectWrap("BACK", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD); 
			else
				GUI_DispStringInRectWrap("PUMP", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			break;
		
		case DRAW_UNITS:
			GUI_DrawBitmap(&bmButtonYellow, BUTTON2_X0, BUTTON_Y0);
			Rect.x0 = BUTTON2_X0;
			Rect.x1 = BUTTON2_X1;
			GUI_DispStringInRectWrap(GUnitNames[GChannel[0].FUnitId], &Rect, GUI_TA_VCENTER |							
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);	
			break;
			
		case DRAW_START:
			Rect.x0 = BUTTON3_X0;
			Rect.x1 = BUTTON3_X1;
			if(GMeasurementsON)
			{
				GUI_DrawBitmap(&bmButtonRed, BUTTON3_X0, BUTTON_Y0);
				GUI_DispStringInRectWrap("STOP", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			}
			else
			{
				GUI_DrawBitmap(&bmButtonGreen, BUTTON3_X0, BUTTON_Y0);
				GUI_DispStringInRectWrap("START", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			}
			break;	
			
		case DRAW_ALL:
			hMem = GUI_MEMDEV_Create(GScreen_sector[5].FXBegin, 
																				 GScreen_sector[5].FYBegin, 
																				 GScreen_sector[5].FXEnd-GScreen_sector[5].FXBegin, 
																				 GScreen_sector[5].FYEnd-GScreen_sector[5].FYBegin);
			GUI_MEMDEV_Select(hMem);
			GUI_Clear();
		
			GUI_DrawBitmap(&bmButtonYellow, BUTTON1_X0, BUTTON_Y0);
			Rect.x0 = BUTTON1_X0;
			Rect.x1 = BUTTON1_X1;
			if(PumpPage.Active)
				GUI_DispStringInRectWrap("BACK", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD); 
			else
				GUI_DispStringInRectWrap("PUMP", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			
			GUI_DrawBitmap(&bmButtonYellow, BUTTON2_X0, BUTTON_Y0);
			Rect.x0 = BUTTON2_X0;
			Rect.x1 = BUTTON2_X1;
			GUI_DispStringInRectWrap(GUnitNames[GChannel[0].FUnitId], &Rect, GUI_TA_VCENTER |							
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);	
			
			Rect.x0 = BUTTON3_X0;
			Rect.x1 = BUTTON3_X1;
			if(GMeasurementsON)
			{
				GUI_DrawBitmap(&bmButtonRed, BUTTON3_X0, BUTTON_Y0);
				GUI_DispStringInRectWrap("STOP", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			}
			else
			{
				GUI_DrawBitmap(&bmButtonGreen, BUTTON3_X0, BUTTON_Y0);
				GUI_DispStringInRectWrap("START", &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			}			
			GUI_MEMDEV_Select(0);
			GUI_MEMDEV_CopyToLCD(hMem);
			GUI_MEMDEV_Delete(hMem);
			DrawLabelsAndTaho();				// Draw labels "Delivery" & "Back Flow", taho field and "RPM".
			break;
			
			
		case DO_NOT_DRAW:
		default:
			break;
	}	
	GDrawButtonsAndLabels = DO_NOT_DRAW;	
	return;
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @DrawFlask
  *	&		@brief  Draw flask with 1 or 2 temperature values and other params.
	* & 	@param  Associated channel. 
	* &						Mode: 1 thermometer or 2.
	* &						x & y:  starting coordinates.  
	* &						width_coeff:  NORMAL, DOUBLE (FLASK_WIDTH*2) or TRIPPLE. 
	* &						flaskHeight:  height in pixels.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void DrawFlask(TChannel channel, TFlaskMode mode, uint16_t x, uint16_t y, TWidth width_coeff, uint8_t flaskHeight)
{	
	float output_value;
	char str[5] ="";
	GUI_SetFont(GUI_FONT_20_ASCII);
	GUI_RECT Rect;
	
	// draw flask borders:
	GUI_SetColor(GUI_BLACK);
	Rect.x0 = x;
	Rect.x1 = x+2;
	Rect.y0 = y-flaskHeight;
	Rect.y1 = y;
	GUI_FillRectEx(&Rect);	
	Rect.x0 = x+FLASK_WIDTH*width_coeff+1;
	Rect.x1 = x+FLASK_WIDTH*width_coeff+3;
	GUI_FillRectEx(&Rect);	
	Rect.x0 = x+3;
	Rect.x1 = x+FLASK_WIDTH*width_coeff;
	Rect.y0 = y-3;
	Rect.y1 = y;
	GUI_FillRectEx(&Rect);
		
	// draw backgound in flask: 
	uint16_t oil_level = (uint16_t)( channel.FOutputValueDiscovery[channel.FUnitId] * (flaskHeight-4)/channel.FTopValue ) + 4;	
	if(oil_level < flaskHeight)
	{
		GUI_SetColor(FM_COLOR_GREY);
		Rect.x0 = x+3;
		Rect.x1 = x+FLASK_WIDTH*width_coeff;
		Rect.y0 = y - flaskHeight+1;
		Rect.y1 = y - oil_level;
		GUI_FillRectEx(&Rect);
		
		// draw label in flask:
		Rect.y1 = y - flaskHeight + 20;
		GUI_SetColor(GUI_BLACK);
		GUI_DispStringInRectWrap(channel.FChannelName, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		
		// draw oil in flask:
		Rect.y0 = y - oil_level;
		Rect.y1 = y - 4;
		for(uint8_t i=3; i<FLASK_WIDTH+width_coeff; i++)
		{
			Rect.x0 = x+3+(i-3)*width_coeff;
			Rect.x1 = x+2+(i-2)*width_coeff;
			GUI_SetColor(GradientOrange[i-3]);
			GUI_FillRectEx(&Rect);
		}
		
		// draw output value in flask:
		if(channel.FOutputValueDiscovery[channel.FUnitId]<1)
			output_value = floor(channel.FOutputValueDiscovery[channel.FUnitId]*100+((float)0.5))/100;
		else if(channel.FOutputValueDiscovery[channel.FUnitId]<999)
			output_value = floor(channel.FOutputValueDiscovery[channel.FUnitId]*10+((float)0.5))/10;
		else
			output_value = 999;
		sprintf (str, "%g", output_value);
		Rect.y0 = y - 23;
		Rect.y1 = y - 3;
		Rect.x0 = x+3;
		Rect.x1 = x+FLASK_WIDTH*width_coeff;
		GUI_SetColor(GUI_BLACK);
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	}
	
	// draw input temperature1:
	if(channel.FOutputTemperature1<200)
		output_value = floor(channel.FOutputTemperature1*10+((float)0.5))/10;
	else
		output_value = 200;
	sprintf (str, "%g", output_value);
	Rect.x0 = x+1;
	Rect.x1 = x+FLASK_WIDTH*width_coeff+2;
	Rect.y0 = y - flaskHeight - 24;
	Rect.y1 = y - flaskHeight - 4;
	GUI_SetColor(FM_COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_DrawRectEx(&Rect);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = x;
	Rect.x1 = x+FLASK_WIDTH*width_coeff+3;
	Rect.y0 = y - flaskHeight - 25;
	Rect.y1 = y - flaskHeight - 3;
	GUI_DrawRectEx(&Rect);

	if(mode == FLASK_2TH)
	{
		// draw internal temperature2:
		if(channel.FOutputTemperature2<200)
			output_value = floor(channel.FOutputTemperature2*10+((float)0.5))/10;
		else
			output_value = 200;
		sprintf (str, "%g", output_value);
		Rect.x0 = x+1;
		Rect.x1 = x+FLASK_WIDTH*width_coeff+2;
		Rect.y0 = y + 4;
		Rect.y1 = y + 22;
		GUI_SetColor(FM_COLOR_WHITE);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_DrawRectEx(&Rect);
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		Rect.x0 = x;
		Rect.x1 = x+FLASK_WIDTH*width_coeff+3;
		Rect.y0 = y + 3;
		Rect.y1 = y +23;
		GUI_DrawRectEx(&Rect);
	}
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @DrawLabelsAndTaho
  *	&		@brief  Draw labels and taho.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void DrawLabelsAndTaho(void)
{	
	// draw labels ==============================:
	GUI_HMEM hMem = GUI_MEMDEV_Create(GScreen_sector[4].FXBegin, 
																		 GScreen_sector[4].FYBegin, 
																		 GScreen_sector[4].FXEnd - GScreen_sector[4].FXBegin, 
																		 GScreen_sector[4].FYEnd - GScreen_sector[4].FYBegin);
	GUI_MEMDEV_Select(hMem);
	GUI_Clear();

	GUI_RECT Rect;
	GUI_SetFont(GUI_FONT_24_ASCII);
	GUI_SetColor(FM_COLOR_BK2);
	Rect.y0 = LABEL_Y0;
	Rect.y1 = LABEL_Y1;
	Rect.x0 = LABEL1_X0;
	Rect.x1 = LABEL1_X1;
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_DispStringInRectWrap("DELIVERY", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
			
	GUI_SetColor(FM_COLOR_BK2);
	Rect.x0 = LABEL2_X0;
	Rect.x1 = LABEL2_X1;
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_DispStringInRectWrap("BACK FLOW", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	
	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);
	
	// draw taho =================================:
	hMem = GUI_MEMDEV_Create(TAHO_BIG_X0, 
													 BUTTON_Y0, 
													 TAHO_BIG_X1 - TAHO_BIG_X0 +1, 
													 BUTTON_Y1 - BUTTON_Y0 + 1);
	GUI_MEMDEV_Select(hMem);
	GUI_Clear();
	
	GUI_SetFont(GUI_FONT_20_ASCII);
	GUI_SetColor(FM_COLOR_BK2);
	Rect.y0 = BUTTON_Y0;
	Rect.y1 = BUTTON_Y1;
	Rect.x0 = TAHO_BIG_X0;
	Rect.x1 = TAHO_BIG_X1;
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	
	GUI_SetColor(FM_COLOR_WHITE);
	Rect.y0 = TAHO_Y0;
	Rect.y1 = TAHO_Y1;
	Rect.x0 = TAHO_X0;
	Rect.x1 = TAHO_X1;
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	
	Rect.x0 = TAHO_X1;
	Rect.x1 = TAHO_BIG_X1;
	GUI_SetColor(GUI_BLACK);
	GUI_DispStringInRectWrap("RPM", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		
	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ToggleUnits
	*	&		@brief  Toggle units: ml/min, ml/10_str ... 
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
static void ToggleUnits(void)
{
	GChannel[0].FUnitId++;
	if(UNITS_CNT<=GChannel[0].FUnitId)
		GChannel[0].FUnitId = 0;
	
	for(uint8_t i=1; i<CHANNEL_NUM; i++)
		GChannel[i].FUnitId = GChannel[0].FUnitId;
}


// --------------------------------------------------------- Private constants -------------------:
// Color list for orange gradient in flask =====:
const uint32_t GradientOrange[FLASK_WIDTH-1] = {
												0x00baf3,
												0x00baf3,
												0x00b7f2,
												0x00b7f2,
												0x00b4f1,
												0x00b1f0,
												0x01adee,
												0x00a9ed,
												0x00a3ec,
												0x009ee9,
												0x0099e7,
												0x0094e6,
												0x018ee3,
												0x0188e2,
												0x0083df,
												0x017ede,
												0x0079dc,
												0x0174db,
												0x0171d9,
												0x006dd8,
												0x0169d7,
												0x0168d6,
												0x0165d5,
												0x0165d5,
												0x0165d5,
												0x0166d5,
												0x0067d6,
												0x006ad7,
												0x0171d9,
												0x0174db,
												0x0079dc,
												0x017ede,
												0x0083df,
												0x0188e2,
												0x018ee3,
												0x0094e6,
												0x0099e7,
												0x009ee9,
												0x00a3ec,
												0x00a9ed,
												0x00a9ed,
												0x01adee,
												0x00b1f0,
												0x00b1f0,
												0x00b4f1,
												0x00b4f1,
												0x00b4f1 };


/**********************************************************************************END OF FILE****/

