/**
  ************************************************************************************************
  * @file	   GeneralPage.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Display channel settings page graphic and react on screen touching.
  *************************************************************************************************
  */
	
#include "GraphicTask/CnannelPage.h"

#include "Application.h"
#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/GraphKeyboard.h"
#include "GraphicTask/SettingsWindow.h"


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ChannelPage_Process
  *	&		@brief  React on button pressing.
  * & 	@param  TouchParameter: ts.
	*	&						page_number: number for displayed channel information choosing.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void ChannelPage_Process(__IO TS_StateTypeDef  ts, uint8_t page_number)
{
	if( FIELD_BIG_X0 < ts.touchX[0] && ts.touchX[0] < FIELD_BIG_X1 )	// Fields...
	{
		// Field1 "Channel_ Delivery" =======================================:
		if( FIELD1_BIG_Y0 < ts.touchY[0] && ts.touchY[0] < FIELD1_BIG_Y1 )	
		{
			GKeyBoard.FMin = 1;			
			GKeyBoard.FMax = 100001;
			if(page_number<5)
				GKeyBoard.Target = &(GChannel[page_number-1].FOval->FCalibration);
			else
				GKeyBoard.Target = &(GChannel[8].FOval->FCalibration);
			GKeyBoard.Active = 1;
			GKeyBoard.Coord[0] = FIELD_X0;
			GKeyBoard.Coord[1] = FIELD_X1;
			GKeyBoard.Coord[2] = FIELD1_Y0;
			GKeyBoard.Coord[3] = FIELD1_Y1;
			GToDrawFLG = 1;			
		}
		// Field2 "Channel_ Back Flow" ===========================================:
		else if( FIELD2_BIG_Y0 < ts.touchY[0] && ts.touchY[0] < FIELD2_BIG_Y1 )	
		{
			GKeyBoard.FMin = 1;			
			GKeyBoard.FMax = 100001;
			if(page_number<5)
				GKeyBoard.Target = &(GChannel[page_number+3].FOval->FCalibration);
			else
				GKeyBoard.Target = &(GChannel[9].FOval->FCalibration);
			GKeyBoard.Active = 1;
			GKeyBoard.Coord[0] = FIELD_X0;
			GKeyBoard.Coord[1] = FIELD_X1;
			GKeyBoard.Coord[2] = FIELD2_Y0;
			GKeyBoard.Coord[3] = FIELD2_Y1;
			GToDrawFLG = 1;
		}
	}
}



/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ChannelPage_Process
  *	&		@brief  Draw all graphics.
  * & 	@param  page_number: number for displayed channel information choosing.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void ChannelPage_Draw(uint8_t page_number)
{	
	GUI_RECT Rect;
	GUI_SetFont(&GUI_Font20_1);
	GUI_SetTextMode(GUI_TM_TRANS);
	char str[10] = "";
	
	// draw BIG fields:
	Rect.x0 = FIELD_BIG_X0;
	Rect.x1 = FIELD_BIG_X1;
	Rect.y0 = FIELD1_BIG_Y0;
	Rect.y1 = FIELD1_BIG_Y1;
	GUI_SetColor(FM_COLOR_BK2);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	
	Rect.y0 = FIELD2_BIG_Y0;
	Rect.y1 = FIELD2_BIG_Y1;
	GUI_SetColor(FM_COLOR_BK2);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	
	Rect.x0 = FIELD_BIG_X0+1;
	Rect.x1 = FIELD_BIG_X1-1;
	Rect.y0 = FIELD1_BIG_Y0+1;
	Rect.y1 = FIELD1_BIG_Y1-1;
	GUI_DrawRectEx(&Rect);
	
	Rect.y0 = FIELD2_BIG_Y0+1;
	Rect.y1 = FIELD2_BIG_Y1-1;
	GUI_DrawRectEx(&Rect);
	
	// draw Small field_1:
	Rect.x0 = FIELD_X0;
	Rect.x1 = FIELD_X1;	
	Rect.y0 = FIELD1_Y0;
	Rect.y1 = FIELD1_Y1;
	GUI_SetColor(FM_COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);	
	// draw TEXT in Small field_1:
	if(page_number<5)
		sprintf(str, "%u", GChannel[page_number-1].FOval->FCalibration);
	else
		sprintf(str, "%u", GChannel[8].FOval->FCalibration); 
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	// draw TEXT in BIG field_1:												
  Rect.y0 = FIELD1_BIG_Y0;
	Rect.y1 = FIELD1_Y0;
	GUI_DispStringInRectWrap("Delivery\nCalibration", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);

		// draw Small field_2:
	Rect.y0 = FIELD2_Y0;
	Rect.y1 = FIELD2_Y1;
	GUI_SetColor(FM_COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(FM_COLOR_DARKBLUE);
	GUI_DrawRectEx(&Rect);	
	// draw TEXT in Small field_2:
	if(page_number<5)
		sprintf(str, "%u", GChannel[page_number+3].FOval->FCalibration);
	else
		sprintf(str, "%u", GChannel[9].FOval->FCalibration);  
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	// draw TEXT in BIG field_2:												
  Rect.y0 = FIELD2_BIG_Y0;
	Rect.y1 = FIELD2_Y0;
	GUI_DispStringInRectWrap("Back Flow\nCalibration", &Rect, GUI_TA_VCENTER |
															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
}




