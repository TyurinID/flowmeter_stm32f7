/**
  *************************************************************************************************
  * @file	 	 GraphicTask.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Here is Graphic Thread function.
  *************************************************************************************************
  */

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"
#include "GUI.h"
#include "stm32746g_discovery_ts.h"
#include <math.h>

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/MainWindow.h"
#include "GraphicTask/SettingsWindow.h"
#include "GraphicTask/ThermometersWindow.h"
#include "GraphicTask/GraphKeyboard.h"

#include "Application.h"

// --------------------------------------------------------- Global variables --------------------:
TActiveWindow GActiveWindow; 			// Number of current window to be displaed.
extern GUI_CONST_STORAGE GUI_BITMAP bmLogoStardex;

// --------------------------------------------------------- Private variables -------------------:
static uint8_t touched_prev;

// --------------------------------------------------------- Global functions --------------------:


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @GraphicThread
  *	&	  @brief  Draws windows on display, executes all buttons pressing.
  * & 	@param  *argument
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void GraphicThread(void const *argument)
{		
  __IO TS_StateTypeDef  ts;  			// Touch scrin state structure.

	// Wait for thermometers to be initialiazed (in I2C Thread) =========:
	osDelay(GRAPHIC_THREAD_DELAY);								
	// Initializes and configures the touch screen functionalities ======:
	BSP_TS_Init(420, 272);		// 420x272 pixels.
	
	// Set background color and fill display ====================:
	GUI_SetBkColor(FM_COLOR_BK1);
  GUI_Clear();
	// All graphics will be drawn on 0 layer ====================:
	GUI_SelectLayer(0);
	// Draw logo "Stardex" (shouldn't be ever redrawn) ==========:
	GUI_DrawBitmap(&bmLogoStardex, 3  , 2);
		
	InitializeScreenSectors();

	// Initialize pararmeters of all windows and keyboard =======:
	MainWin_Init();
	SettingsWin_Init();	
	ThermometersWin_Init();
	KeyBoard_Init();
	
	// Start with Main Window ===================================:
	GActiveWindow = MAIN_WIN;
	
	// Scrin hasn't been touched ================================: 
	touched_prev = 0;
				
  while(1)
	{
		// Get touch screen parameters ============================:
		BSP_TS_GetState((TS_StateTypeDef *)&ts);
		uint8_t touchDetected = ts.touchDetected;
		
		// Go to Active Window Process ============================:		
		switch(GActiveWindow)				
		{
			case SETTINGS_WIN:
				SettingsWin_Process(touched_prev, ts);
				break;
			case THERMOMETERS_WIN:
				ThermometersWin_Process(touched_prev, ts);
				break;
			case MAIN_WIN:
			default:
				MainWin_Process(touched_prev, ts);
				break;
		}
				
		touched_prev = touchDetected;
			
		osDelay(20);
	}
}


/**********************************************************************************END OF FILE****/
