/**
  ************************************************************************************************
  * @file	   GraphKeyboard.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Initialization of keyboard buttons.
	*					 Buttons pressing reaction.
  *************************************************************************************************
  */
	
	// ########################################################## Includes ############################:
#include "GraphicTask/GraphKeyboard.h"
#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/SettingsWindow.h"

// ################################################# Global variables #############################:
TKeyBoard GKeyBoard;			

// --------------------------------------------------------- Private variables -------------------:
static GUI_RECT key[15];					// Rectangular buttons.
static uint32_t dummy_value;			
static float 		dummy_value2=0.0;


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @KeyBoard_Init
  *	&		@brief  Configure all coordinates and default settings.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void KeyBoard_Init(void)
{
	GKeyBoard.Active = 0;
	GKeyBoard.Mode = KB_NORMAL;
	GKeyBoard.digits_length = 0;
	GKeyBoard.Target = &dummy_value;
	GKeyBoard.AdditTarget = &dummy_value2; 
	GKeyBoard.FMin = 0;
	GKeyBoard.Coord[0] = 60;
	GKeyBoard.Coord[1] = 160;
	GKeyBoard.Coord[2] = 80;
	GKeyBoard.Coord[3] = 115;
	
	// temp
	GKeyBoard.digits[0] = 1;
	GKeyBoard.digits[1] = 2;
	GKeyBoard.digits[2] = 3;
	
	key[1].x0 = COLUMN1_X0;
	key[1].y0 = LINE1_Y0;
	key[1].x1 = COLUMN1_X1;
	key[1].y1 = LINE1_Y1;
	
	key[2].x0 = COLUMN2_X0;
	key[2].y0 = LINE1_Y0;
	key[2].x1 = COLUMN2_X1;
	key[2].y1 = LINE1_Y1;
	
	key[3].x0 = COLUMN3_X0;
	key[3].y0 = LINE1_Y0;
	key[3].x1 = COLUMN3_X1;
	key[3].y1 = LINE1_Y1;
	
	key[4].x0 = COLUMN1_X0;
	key[4].y0 = LINE2_Y0;
	key[4].x1 = COLUMN1_X1;
	key[4].y1 = LINE2_Y1;

	key[5].x0 = COLUMN2_X0;
	key[5].y0 = LINE2_Y0;
	key[5].x1 = COLUMN2_X1;
	key[5].y1 = LINE2_Y1;
	
	key[6].x0 = COLUMN3_X0;
	key[6].y0 = LINE2_Y0;
	key[6].x1 = COLUMN3_X1;
	key[6].y1 = LINE2_Y1;
	
	key[7].x0 = COLUMN1_X0;
	key[7].y0 = LINE3_Y0;
	key[7].x1 = COLUMN1_X1;
	key[7].y1 = LINE3_Y1;
	
	key[8].x0 = COLUMN2_X0;
	key[8].y0 = LINE3_Y0;
	key[8].x1 = COLUMN2_X1;
	key[8].y1 = LINE3_Y1;
	
	key[9].x0 = COLUMN3_X0;
	key[9].y0 = LINE3_Y0;
	key[9].x1 = COLUMN3_X1;
	key[9].y1 = LINE3_Y1;
	
	key[0].x0 = COLUMN2_X0;
	key[0].y0 = LINE4_Y0;
	key[0].x1 = COLUMN2_X1;
	key[0].y1 = LINE4_Y1;
	
	key[BCKSP_POS].x0 = COLUMN1_X0;
	key[BCKSP_POS].y0 = LINE4_Y0;
	key[BCKSP_POS].x1 = COLUMN1_X1;
	key[BCKSP_POS].y1 = LINE4_Y1;
	
	key[ENTER_POS].x0 = COLUMN3_X0;
	key[ENTER_POS].y0 = LINE4_Y0;
	key[ENTER_POS].x1 = COLUMN3_X1;
	key[ENTER_POS].y1 = LINE4_Y1;
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @KeyBoard_Process
  *	&		@brief  React on button pressing.
  * & 	@param  Touch screen structure ts.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void KeyBoard_Process(__IO TS_StateTypeDef  ts)
{
	uint8_t key_pressed = 0xff;
	for(uint8_t i=0; i<12; i++)
	{
		if(key[i].x0 < ts.touchX[0] && 
			 key[i].x1 > ts.touchX[0] &&
		   key[i].y0 < ts.touchY[0] &&
			 key[i].y1 > ts.touchY[0] )
			 key_pressed = i;
	}
	
	if( 10> key_pressed)			// digit pressed.
	{
		if(KEY_BOARD_DIGITS>GKeyBoard.digits_length)
		{
			GKeyBoard.digits[GKeyBoard.digits_length] = key_pressed;
			GKeyBoard.digits_length++;
			GToDrawFLG = 1;												// Redraw display.
			return;
		}
	}
	
	if(BCKSP_POS == key_pressed)		// bcsp pressed.
	{
		if(0<GKeyBoard.digits_length)
			GKeyBoard.digits_length--;
		GToDrawFLG = 1;												// Redraw display.
		return;
	}
		
	// ESC if frame pressed:
	if(GKeyBoard.Coord[0] < ts.touchX[0] && 
			 ts.touchX[0] < GKeyBoard.Coord[1] &&
		   GKeyBoard.Coord[2] < ts.touchY[0] &&
			 ts.touchY[0] < GKeyBoard.Coord[3] )
	{
	GKeyBoard.digits_length = 0;
	GKeyBoard.Coord[0] = 60;
	GKeyBoard.Coord[1] = 160;
	GKeyBoard.Coord[2] = 80;
	GKeyBoard.Coord[3] = 115;
	GKeyBoard.Mode = KB_NORMAL;
	GKeyBoard.Active = 0;
	GToDrawFLG = 1;												// Redraw display.
	return;
	}
	
	if(ENTER_POS == key_pressed)
	{
		uint32_t temp = 0;
		uint32_t decimals = 1;
		
		for(uint8_t i=0; i<(GKeyBoard.digits_length-1); i++)
			decimals = decimals*10;
		for(uint8_t i=0; i<GKeyBoard.digits_length; i++)
		{
			temp += GKeyBoard.digits[i]*decimals;
		  decimals = decimals/10;
		}
			
		if(temp<=GKeyBoard.FMax && temp>=GKeyBoard.FMin)
		{
			*(GKeyBoard.Target) = temp;
		}
		GKeyBoard.digits_length = 0;
		GKeyBoard.Coord[0] = 60;
		GKeyBoard.Coord[1] = 160;
		GKeyBoard.Coord[2] = 80;
		GKeyBoard.Coord[3] = 115;
		GKeyBoard.Mode = KB_NORMAL;
		GKeyBoard.Active = 0;
		GToDrawFLG = 1;												// Redraw display.
		return;
	}
}


void KeyBoard_Draw(void)
{
	char str[10];
	GUI_RECT RectBck;
	RectBck.x1 = 467;
	RectBck.y0 = 0;
	RectBck.y1 = 224;
	
	// Rectangular background
	if(GKeyBoard.Mode == KB_DRAW_FRAME || GKeyBoard.Mode == KB_DRAW_ADDIT)
		RectBck.x0 = 50;
	else
		RectBck.x0 = 190;
	
	GUI_SetColor(FM_COLOR_BK1);
	GUI_FillRectEx(&RectBck);
	
	GUI_SetFont(GUI_FONT_24_1);
	
	for(uint8_t i=0; i<12; i++)	// draw keys
	{
		GUI_SetColor(FM_COLOR_WHITE);
		GUI_FillRectEx(&key[i]);
		GUI_SetColor(FM_COLOR_DARKBLUE);
		GUI_DrawRectEx(&key[i]);
		GUI_SetTextMode(GUI_TM_TRANS);
		if(10>i)
		{
			sprintf(str, "%d", i);
		}
		else
		{
			switch(i)
			{
				case BCKSP_POS:
					sprintf(str, "Bcksp");
					break;
				case ENTER_POS:
					sprintf(str, "Enter");
					break;
				default:
					break;
			}
		}
		GUI_DispStringAt(str, (key[i].x0+key[i].x1)/2 - GUI_GetStringDistX(str)/2,  (key[i].y0+key[i].y1)/2 -GUI_GetFontSizeY()/2 );
		// TI: locate name of button in the rectangle centre
	}
	
	GUI_SetFont(GUI_FONT_20_1);
	
	// draw frame
	GUI_RECT Rect = {GKeyBoard.Coord[0]+1, GKeyBoard.Coord[2]+1, GKeyBoard.Coord[1]-1, GKeyBoard.Coord[3]-1};
	GUI_SetColor(GUI_RED);
	GUI_DrawRoundedFrame(GKeyBoard.Coord[0] - 4, GKeyBoard.Coord[2] - 4, GKeyBoard.Coord[1] + 4, GKeyBoard.Coord[3] + 4, 4, 2);
	GUI_SetColor(FM_COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	for(uint8_t i =0; i <GKeyBoard.digits_length; i++)
	{
		str[i] = GKeyBoard.digits[i] + '0';
	}
	str[GKeyBoard.digits_length] = '\0';
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	
	if(GKeyBoard.Mode == KB_DRAW_ADDIT)
	{
		// 	draw added inf:
		Rect.y0 = 140;
		Rect.y1 = 180;
		sprintf(str, "%g", *(GKeyBoard.AdditTarget));
		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	}
}




