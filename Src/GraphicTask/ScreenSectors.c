/**
  ************************************************************************************************
  * @file	   ScreenSectors.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Initialization of screen sectors.
  *************************************************************************************************
  */

#include "GraphicTask/ScreenSectors.h"

TScreenSector GScreen_sector[SCREEN_SECTORS_NUM];


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @InitializeScreenSectors
  *	&		@brief  Configure Screen sectors coordinates.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void InitializeScreenSectors(void)
{
	GScreen_sector[0].FXBegin = 45;
	GScreen_sector[0].FXEnd   = 155;
	GScreen_sector[0].FYBegin = 26;  
	GScreen_sector[0].FYEnd 	= 227;
	
	GScreen_sector[1].FXBegin = 153;
	GScreen_sector[1].FXEnd   = 263;
	GScreen_sector[1].FYBegin = 26;  
	GScreen_sector[1].FYEnd 	= 227;

	GScreen_sector[2].FXBegin = 261;
	GScreen_sector[2].FXEnd   = 371;
	GScreen_sector[2].FYBegin = 26;  
	GScreen_sector[2].FYEnd 	= 227;	
	
	GScreen_sector[3].FXBegin = 369;
	GScreen_sector[3].FXEnd   = 480;
	GScreen_sector[3].FYBegin = 26;  
	GScreen_sector[3].FYEnd 	= 227;
	
	GScreen_sector[4].FXBegin = 45;
	GScreen_sector[4].FXEnd   = 480;
	GScreen_sector[4].FYBegin = 1;  
	GScreen_sector[4].FYEnd 	= 27;
	
	GScreen_sector[5].FXBegin = 1;
	GScreen_sector[5].FXEnd   = 479;
	GScreen_sector[5].FYBegin = 227;  
	GScreen_sector[5].FYEnd 	= 271;	
}
