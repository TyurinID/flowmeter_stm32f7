

#include "ModbusTCP/ModbusTCP.h"

#include "TagSystem/ModbusPDU.h"

#include "lwip/sockets.h"

uint8_t GIpAddrConfigured=0;

TIpAddr GIpAddress;
TIpAddr GNetMask;
TIpAddr GDefaultGateway;	

static uint8_t transmit_buf[256];
static uint16_t tr_buf_size;
static unsigned char recv_buffer[1500];

struct netif gnetif; /* network interface structure */

/**
  * @brief  Initializes the lwIP stack
  * @param  None
  * @retval None
  */
static void Netif_Config(void)
{
  struct ip_addr ipaddr;
  struct ip_addr netmask;
  struct ip_addr gw;	
	
	while(0==GIpAddrConfigured)
	{
		osDelay(10);
	}
  
  /* IP address setting */
  IP4_ADDR(&ipaddr, (uint8_t)(GIpAddress.byte1), (uint8_t)(GIpAddress.byte2), (uint8_t)(GIpAddress.byte3), (uint8_t)(GIpAddress.byte4));
  IP4_ADDR(&netmask, (uint8_t)(GNetMask.byte1), (uint8_t)(GNetMask.byte2) , (uint8_t)(GNetMask.byte3), (uint8_t)(GNetMask.byte4));
  IP4_ADDR(&gw, (uint8_t)(GDefaultGateway.byte1), (uint8_t)(GDefaultGateway.byte2), (uint8_t)(GDefaultGateway.byte3), (uint8_t)(GDefaultGateway.byte4) );
  
  /* - netif_add(struct netif *netif, struct ip_addr *ipaddr,
  struct ip_addr *netmask, struct ip_addr *gw,
  void *state, err_t (* init)(struct netif *netif),
  err_t (* input)(struct pbuf *p, struct netif *netif))
  
  Adds your network interface to the netif_list. Allocate a struct
  netif and pass a pointer to this structure as the first argument.
  Give pointers to cleared ip_addr structures when using DHCP,
  or fill them with sane numbers otherwise. The state pointer may be NULL.
  
  The init function pointer must point to a initialization function for
  your ethernet netif interface. The following code illustrates it's use.*/
  
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);
  
  /*  Registers the default network interface. */
  netif_set_default(&gnetif);
  
  /*if (netif_is_link_up(&gnetif))
  {
    netif_set_up(&gnetif);
  }
  else
  {
    netif_set_down(&gnetif);
  }*/
	
	if (0==netif_is_link_up(&gnetif))
	{
		osDelay(10);
	}
	netif_set_up(&gnetif);
}

static bool tcp_reload;

static void ModbusAppCall(int conn, bool* tcp_rel) 
{
	uint16_t incoming_packet_length;
	uint16_t outcoming_packet_length;
  
	int buflen = 1500;
  int ret;
  
	/* Read in the request */
  ret = read(conn, recv_buffer, buflen); 
  if(ret < 0) 
		return;
	
	if(0 == ret)
		*tcp_rel =true;
	
	// if success, generate answer
	{	
				// generate adu
			transmit_buf[0] = recv_buffer[0];  // copy message id
			transmit_buf[1] = recv_buffer[1];
			transmit_buf[2] = recv_buffer[2];  // copy protocol id
			transmit_buf[3] = recv_buffer[3];
			//transmit_buf[4] = 0;
			//transmit_buf[5] = 5;                  // set data length
			transmit_buf[6] = recv_buffer[6];  // copy unit id
			
				// generate pdu
			incoming_packet_length = ((uint16_t)(recv_buffer[4]))<<8 | (uint16_t)(recv_buffer[5]);
			MakeModbusPdu(&(recv_buffer[7]), incoming_packet_length, &(transmit_buf[7]), &outcoming_packet_length);
			
			transmit_buf[4] = (uint8_t)((outcoming_packet_length+1) >> 8);
			transmit_buf[5] = (uint8_t)((outcoming_packet_length+1) & 0x00ff);
			tr_buf_size = 7 + outcoming_packet_length;
			
			write(conn, (const uint8_t*)transmit_buf, (size_t)tr_buf_size);
	}
		
}


void ModbusTCPThread(void const *argument)
{
	int sock, newconn, size;
	struct sockaddr_in address, remotehost;
	
	/* Create tcp_ip stack thread */
  tcpip_init(NULL, NULL);
  
  /* Initialize the LwIP stack */
  Netif_Config(); 
	
	/* create a TCP socket */
  while ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
  {
		osDelay(10);
   // return;
  }
  
  /* bind to port 502 at any interface */
  address.sin_family = AF_INET;
  address.sin_port = htons(502);
  address.sin_addr.s_addr = INADDR_ANY;

  while (bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
  {
		osDelay(10);
    //return;
  }
  
  /* listen for incoming connections (TCP listen backlog = 5) */
  listen(sock, 5);
  
  size = sizeof(remotehost);
	
	while(1)
	{
		tcp_reload = false;
		newconn = accept(sock, (struct sockaddr *)&remotehost, (socklen_t *)&size);
		while (false == tcp_reload) 
		{
			ModbusAppCall(newconn, &tcp_reload);
		}
		/* Close connection socket */
		close(newconn);
	}
}






