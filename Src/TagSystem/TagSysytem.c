
#include "TagSystem/TagSystem.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"



//output holding registers

TUint16Tag ModbusOutputHoldingRegisters[MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT];


TUint16Tag* CreateUint16Tag(uint16_t *TagSource, uint16_t Index)  // create non-retained uint16 tag
{
	ModbusOutputHoldingRegisters[Index].FValue = TagSource;
	
	#ifdef USE_RETAINED_TAGS
	ModbusOutputHoldingRegisters[Index].FRetained = false;
	#endif
	
	return (&ModbusOutputHoldingRegisters[Index]);
}

#ifdef USE_RETAINED_TAGS
TUint16Tag* CreateUint16RetainedTag(uint16_t *TagSource, uint16_t EepromAddress, uint16_t Index) // create retained uint16_t tag
{
	ModbusOutputHoldingRegisters[Index].FValue = TagSource;
	ModbusOutputHoldingRegisters[Index].FRetained = true;
	ModbusOutputHoldingRegisters[Index].FEepromAdress = EepromAddress;
	
	//GetUint16TagValue(ModbusOutputHoldingRegisters[Index]);
	
	uint32_t temp_value;
	taskENTER_CRITICAL();
	EEPROMRead(&temp_value, ModbusOutputHoldingRegisters[Index].FEepromAdress, sizeof(temp_value));
	taskEXIT_CRITICAL();
	
	uint16_t rounded_adress = (EepromAddress/4)*4;
	if(EepromAddress == rounded_adress)
	{
		*(ModbusOutputHoldingRegisters[Index].FValue) = (uint16_t)(temp_value);
	}
	else
	{
		*(ModbusOutputHoldingRegisters[Index].FValue) = (uint16_t)(temp_value>>16);
	}
	
	return (&ModbusOutputHoldingRegisters[Index]);
}
#endif

uint16_t GetUint16TagValue(TUint16Tag tag)  // get value of the tag 
{
	return *(tag.FValue);
}

void SetUint16TagValue(TUint16Tag tag, uint16_t value)  // set tag value
{
	*(tag.FValue) = value;
	
	#ifdef USE_RETAINED_TAGS
	if(tag.FRetained)
	{
		*(tag.FValue) = value;
		
		uint16_t rounded_adress = (tag.FEepromAdress/4)*4;
		uint32_t temp_in_value, temp_out_value;
		
		taskENTER_CRITICAL();
		EEPROMRead(&temp_in_value, tag.FEepromAdress, sizeof(temp_in_value));
		taskEXIT_CRITICAL();
		
		if(rounded_adress == tag.FEepromAdress)
		{
			temp_out_value = temp_in_value & 0xffff0000;
			temp_out_value |= (uint32_t)value;
		}
		else
		{
			temp_out_value = temp_in_value & 0x0000ffff;
			temp_out_value |= ((uint32_t)value)<<16;
		}
		
	  //temp_out_value = (uint32_t)value;
		taskENTER_CRITICAL();
		EEPROMProgram(&temp_out_value, tag.FEepromAdress, sizeof(temp_out_value));
		taskEXIT_CRITICAL();
	}
	#endif
}



// input registers

TUint16Tag ModbusInputRegisters[MODBUS_INPUT_REGISTERS_COUNT];

TUint16Tag* CreateUint16InputTag(uint16_t *TagSource, uint16_t Index)
{
	ModbusInputRegisters[Index].FValue = TagSource;
	
	#ifdef USE_RETAINED_TAGS
	ModbusInputRegisters[Index].FRetained = false;
	#endif
	
	return (&ModbusInputRegisters[Index]);
}

// discrete coils
TBoolTag ModbusDiscreteCoils[MODBUS_DISCRETE_COILS_COUNT];

TBoolTag *CreateBoolTag(bool *TagSource, uint16_t Index)
{
	ModbusDiscreteCoils[Index].FValue = TagSource;
	
	return (&ModbusDiscreteCoils[Index]);
}

uint8_t GetBoolTagValueBitmask(TBoolTag *table, uint16_t StartAdress, uint8_t Count)
{
	uint8_t RealCount, res;
	if(Count>8)
		RealCount = 8;
	else
		RealCount = Count;
	
	res = 0;
	
	for(uint8_t i=0; i<RealCount; i++)
	{
		if(true == (*(table[StartAdress+i].FValue)) )
		{
			res |= (1<<i);
		}
	}
	
	return res;
}

void SetBoolValueBitmask(TBoolTag *table, uint8_t Bitmask, uint8_t Count)
{
	uint8_t RealCount;
	if(Count>8)
		RealCount = 8;
	else
		RealCount = Count;

	for(uint8_t i=0; i<RealCount; i++)
	{
		if( Bitmask & (1<<i) )
			*(table[i].FValue) = true;
		else
			*(table[i].FValue) = false;
	}
}

// discrete inputs
TBoolTag ModbusDiscreteInputs[MODBUS_DISCRETE_INPUTS_COUNT];

TBoolTag *CreateBoolInputTag(bool *TagSource, uint16_t Index)
{
	ModbusDiscreteInputs[Index].FValue = TagSource;
	
	return (&ModbusDiscreteInputs[Index]);
}

// tag padding
static uint16_t padding_reg = 0;
static bool padding_coil=0;

void MakeTagsPadding(void)
{
	for(uint8_t i=0; i<MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT; i++)
		ModbusOutputHoldingRegisters[i].FValue = &padding_reg;
	for(uint8_t i=0; i<MODBUS_INPUT_REGISTERS_COUNT; i++)
		ModbusInputRegisters[i].FValue = &padding_reg;
	for(uint8_t i=0; i<MODBUS_DISCRETE_COILS_COUNT; i++)
		ModbusDiscreteCoils[i].FValue = &padding_coil;
	for(uint8_t i=0; i<MODBUS_DISCRETE_INPUTS_COUNT; i++)
		ModbusDiscreteInputs[i].FValue = &padding_coil;
}


