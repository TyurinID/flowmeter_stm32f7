/**
  ************************************************************************************************
  * @file	   Application.c
  * @author  Kortev Oleg
  * @version V3.2
  * @date    2016
  * @brief   ApplicationThread do all calculations with data from sensors.
  *************************************************************************************************
  */
// -------------------------------------------------------- Includes -----------------------------:
#include "Application.h"

#include "ModbusTCP/ModbusTCP.h"
#include "GraphicTask/MainWindow.h"
#include "GraphicTask/LogPage.h"
#include "Flash/FlashMap.h"
#include "TagSystem/TagSystem.h"

// --------------------------------------------------------- Global variables --------------------:
TTaho GTaho;
TChannel GChannel[CHANNEL_NUM];		// Sensors Channels.
TValve GValve[VALVE_CNT]; 

uint8_t GMeasurementsON;		// MeasurementsON FLG for displaying output values on discovery screen.
uint32_t	GDeviceWorkingTime;	// Time from first calculation start.
// Modbus discrete coils:
bool GApplicationRun;			  // Command to start measurement
bool GApplicationStop;			// Command to stop measurement
bool GApplicationMeasurementReady;	
// Modbus holding register:
uint32_t GOvalBuffSize;			// Max size of Oval sensor buffer 1..512.
uint16_t GFlashMapSize;	
uint32_t GFanON_Temperature;	// Critical value of themperature.	
TValvesMode GValvesMode;			// Mode of two valves: delivery mode or back flow.		

// --------------------------------------------------------- Private variables -------------------:  

static uint16_t flow_meter_code = 0xaabb;			// for 10/18 thermometers

static uint16_t taho_measure_cnr;							// for timing
static uint16_t max_value_cnr;								// for timing


// --------------------------------------------------------- Private function prototypes ---------:	
static void CheckApplication(void);		// Check all parameters for being in range.
static void ApplicationInit(void);		// Initialize all 
static void CreateModbusTags(void);		// Bind modbus registers with variables in RAM.
static uint16_t FlashMapInit(void);	
static TValvesMode ValvesMode_Prev;		// Preveous state of this register.


// --------------------------------------------------------- Global functions --------------------: 

/***********************@ApplicationThread
  *		@brief  ApplicationThread do all calculations with data from sensors.
  *  	@param  *argument
  *  	@retval None
  **********************/
void ApplicationThread(void const *argument)
{	
	TickType_t xLastWakeTime;
	char str[30] = "";
	
	// Initialize all parameters:
	OvalInit();
	ApplicationInit();
	// Bind modbus registers with variables in RAM:
	CreateModbusTags();
	// Bind Flash with variables in RAM:
	GFlashMapSize = FlashMapInit();
	RecallFlashMap(GFlashMapSize);	// Get all parameters from Flash.
	// Check all parameters for being in range:
	CheckApplication();
	
	GIpAddrConfigured = 1;	// TI?
	
	taho_measure_cnr = 0;
	max_value_cnr = 0;
	
	// Wait for thermometers to be initialiazed (in I2C Thread):	
	osDelay(APPLICATION_THREAD_DELAY); 
		
	// Reset Pulse information:
	for(uint8_t i = 0; i<CHANNEL_NUM; i++)
	{
		taskENTER_CRITICAL();
		GChannel[i].FOval->FPulse = 0;
		taskEXIT_CRITICAL();
	}
	
	xLastWakeTime = xTaskGetTickCount();
	
	while(1)
	{
		float maxTemperature = 0.0;
		for(uint8_t i=0; i< CHANNEL_NUM; i++)	
		{
			// For temperature sensors on device Inlet ================================:
			if(0 == get_sensor_not_ready(*GChannel[i].FTemperatureSensor1) )
			{
				GChannel[i].FOutputTemperature1 = (GChannel[i].FTemperatureSensor1)->CTemperature;		// TI.
				GChannel[i].FTemperatureSensor1Ready = 1;
				
				if(maxTemperature < GChannel[i].FOutputTemperature1)
					maxTemperature = GChannel[i].FOutputTemperature1;
			}
			else
			{
				GChannel[i].FTemperatureSensor1Ready = 0;
			}
			
			// For temperature sensors on device Outlet ================================:
			if(G_ds18b20_Count > CHANNEL_NUM)		// Thermo_2 sensors polling only if thermo bus 1 present :
			{
				if(0 == get_sensor_not_ready(*GChannel[i].FTemperatureSensor2) )
				{
					GChannel[i].FOutputTemperature2 = (GChannel[i].FTemperatureSensor2)->CTemperature;		// TI.
					GChannel[i].FTemperatureSensor2Ready = 1;					
				}
				else
				{
					GChannel[i].FTemperatureSensor2Ready = 0;
				}
			}
		}
		// Turn ON fan if temperature is too high:
		if((uint32_t)maxTemperature > GFanON_Temperature+1)
			GValve[VALVE_CNT - 1].FValue = 1;
		else if((uint32_t)maxTemperature < GFanON_Temperature-1)
			GValve[VALVE_CNT - 1].FValue = 0;		
		
	

		
		
		// Calculate Taho output value periodicaly ==================================:
		taho_measure_cnr++;				
		if(taho_measure_cnr > TAHO_UPDATE_PERIOD/APPLICATION_PERIOD_MS)
		{	
			GTaho.FTimeBuffer[GTaho.FBufferPos] = GTaho.FTimeCnr;						
			GTaho.FBufferPos++;
			if(TAHO_BUF_SIZE<=GTaho.FBufferPos)
				GTaho.FBufferPos = 0;
			
			uint64_t average = 0;
			uint8_t valid_times=0;
			for(uint8_t j=0; j <TAHO_BUF_SIZE; j++)
			{
				if(0!=GTaho.FTimeBuffer[j])
				{
					average += GTaho.FTimeBuffer[j];
					valid_times++;
				}
			}
			average /= valid_times;
			
			if(50000>average)
				GTaho.FOutput = ((float)600000)/((float)average);
			else
				GTaho.FOutput = 0;
			taho_measure_cnr = 0;
		}
		
		
		
		// Calculate Oval output value if pulse =======================================:
		GDeviceWorkingTime++;		
		for(uint8_t i=0; i<CHANNEL_NUM; i++)
		{
			if(GChannel[i].FOval->FPulse == 1)				// If pulse from sensor, Update Buffer.
			{
				GChannel[i].FOval->FPulse = 0;
				
				if(GChannel[i].FMeasureStartTimer >= 1)		// Wait for second pulse until measure.
				{
					UpdateOval(GChannel[i].FOval);
							
					for(uint8_t j=0; j<UNITS_CNT; j++)			// Get output value for Modbus. 
						GChannel[i].FOutputValue[j] = CountOvalOutput(GChannel[i].FOval, j, GTaho.FOutput);		
					if(GMeasurementsON)													
					{	
						for(uint8_t j=0; j<UNITS_CNT; j++)		// Get output value for Discovery display. 
							GChannel[i].FOutputValueDiscovery[j] = GChannel[i].FOutputValue[j];
					}
				}	
				
				if(GChannel[i].FMeasureStartTimer < 5)		// Log first 5 pulses.
				{
					GChannel[i].FMeasureStartTimer++;				// Wait for third pulse.
					if(GMeasurementsON)
					{
						sprintf(str,"%.1fs  pulse %d", (float)GDeviceWorkingTime/100, GChannel[i].FMeasureStartTimer);
						AddLog(&GChannel[i], str);	
					}
				}
			}
			// Log output value each LOG_OUTPUT_VALUE_PERIOD ms.
			if((GDeviceWorkingTime % (LOG_OUTPUT_VALUE_PERIOD/APPLICATION_PERIOD_MS)) == 0 && GMeasurementsON)
			{	
				sprintf(str,"%.1fs   d: %.1f", (float)GDeviceWorkingTime/100, GChannel[i].FOutputValue[0]);
					AddLog(&GChannel[i], str);	
			}
		}
		
		// Calculate top channel values for displaing in flasks ================================:
		SetOutputMaximums();		
		
		// Get new commands from Modbus registers and execute them =============================:
		if(GApplicationRun)	// Command to start measurement
		{
			for(uint8_t i=0; i<CHANNEL_NUM; i++)
			{
				sprintf(str,"%.1fs    RESET", (float)GDeviceWorkingTime/100 );
				AddLog(&GChannel[i], str);
				GChannel[i].FMeasureStartTimer = 0;
				ResetOutputValues(&GChannel[i]);
			}
			for(uint8_t i=0; i<CHANNEL_NUM; i++)
			{
				ResetOval(GChannel[i].FOval);
			}
			GMeasurementsON = 1;	
			GApplicationRun = 0;
			GDrawButtonsAndLabels = DRAW_ALL;
		}
		
		if(GApplicationStop) // Command to stop measurement
		{
			for(uint8_t i=0; i<CHANNEL_NUM; i++)
			{				
				sprintf(str,"%.1fs    STOP", (float)GDeviceWorkingTime/100 );
					AddLog(&GChannel[i], str);					
			}
			GMeasurementsON = 0;
			GApplicationStop = 0;
			GDrawButtonsAndLabels = DRAW_ALL;
		}
		
		// Read GValvesMode register and if changed, reset valves configuration ====:
		if(GValvesMode != ValvesMode_Prev)
		{
			ValvesMode_Prev = GValvesMode;
			switch(ValvesMode_Prev)
			{
				default:
				case DELIVERY_MODE:
					GValve[0].FValue = SET;
					GValve[1].FValue = RESET;
					break;
				
				case BACK_FLOW_MODE:
					GValve[0].FValue = RESET;
					GValve[1].FValue = SET;
					break;
				
				case BOTH_TURNED_OFF:
					GValve[0].FValue = RESET;
					GValve[1].FValue = RESET;
					break;
				
				case BOTH_TURNED_ON:
					GValve[0].FValue = SET;
					GValve[1].FValue = SET;
					break;
			}
		}
		
		GApplicationMeasurementReady = 1;
		
		vTaskDelayUntil(&xLastWakeTime, APPLICATION_PERIOD_MS);			// Period of application.
	}
}




/***********************@ResetOutputValues
  *		@brief  Reset output channel values and clear oval sensor buffer.
  *  	@param  None
  *  	@retval None
  **********************/
void ResetOutputValues(TChannel* channel)
{
	channel->FTopValue = 0.5;
	for(uint8_t j=0; j<UNITS_CNT; j++)		// Get output value for Discovery display. 
	{
		channel->FOutputValue[j] = 0.0;
		channel->FOutputValueDiscovery[j] = 0.0;
	}
}


/***********************@SetOutputMaximums
  *		@brief  Calculate top channel values for displaing in flasks.
  *  	@param  None
  *  	@retval None
  **********************/
void SetOutputMaximums(void)
{
	#ifdef USE_LOCAL_MAXIMUMS
	
	max_value_cnr++;		// calculate local maximums for flasks:
	if(max_value_cnr > TOP_VALUE_UPDATE_PERIOD/APPLICATION_PERIOD_MS)
	{				
		float local_max = (float)1.0;
		for(uint8_t i=0; i<8; i++)
		{
			if(local_max < GChannel[i].FOutputValueDiscovery[GChannel[i].FUnitId])
				local_max = GChannel[i].FOutputValueDiscovery[GChannel[i].FUnitId];
		}
		if((float)(GChannel[0].FTopValue*0.9f) <= local_max  ||  (float)(GChannel[0].FTopValue*0.35f) > local_max)
		{
			for(uint8_t i=0; i<8; i++)
				GChannel[i].FTopValue = local_max*2;
		}
	
		local_max = (float)1.0;
		for(uint8_t i=8; i<10; i++)
		{
			if(local_max < GChannel[i].FOutputValueDiscovery[GChannel[i].FUnitId])
				local_max = GChannel[i].FOutputValueDiscovery[GChannel[i].FUnitId];
		}
		if((float)(GChannel[8].FTopValue*0.9f) <= local_max  ||  (float)(GChannel[8].FTopValue*0.35f) > local_max)
		{
			for(uint8_t i=8; i<10; i++)
				GChannel[i].FTopValue = local_max*2;
		}
	}
	
	#else
	for(uint8_t i=0; i<CHANNEL_NUM; i++)
		GChannel[i].FTopValue = (float)400.0;
	#endif
}




// --------------------------------------------------------- Private functions -----------------------:

/***********************@CheckApplication
  *		@brief  Check all parameters for being in range.
  *  	@param  None
  *  	@retval None
  **********************/
static void CheckApplication(void)
{
	if(255<=GIpAddress.byte1  &&  255<=GIpAddress.byte2
		&&	255<=GIpAddress.byte3  &&  255<=GIpAddress.byte4)
	{
		GIpAddress.byte1 =192;	//192
		GIpAddress.byte2 =168;	//168
		GIpAddress.byte3 =0;		// 0
		GIpAddress.byte4 =201;	// 201
	}
	
	if(255<=GNetMask.byte1	&&	255<=GNetMask.byte2
		&&	255<=GNetMask.byte3  &&  255<=GNetMask.byte4)
	{	
		GNetMask.byte1 =255;
		GNetMask.byte2 =255;
		GNetMask.byte3 =255;		
		GNetMask.byte4 =0;			// 0
	}
	
	if(255<GDefaultGateway.byte1) GDefaultGateway.byte1 =255;
	if(255<GDefaultGateway.byte2) GDefaultGateway.byte2 =255;
	if(255<=GDefaultGateway.byte3) GDefaultGateway.byte3 =0;
	if(255<=GDefaultGateway.byte4) GDefaultGateway.byte4 =1;
	
	for(uint8_t i=0; i<CHANNEL_NUM; i++)
	{
		if(OVAL_MAX_CALIBRATION<GChannel[i].FOval->FCalibration)
			GChannel[i].FOval->FCalibration = OVAL_MAX_CALIBRATION;
		if(0 == GChannel[i].FOval->FCalibration)
			GChannel[i].FOval->FCalibration = 1;
		if(100000 < GChannel[i].FOval->FCalibration)
			GChannel[i].FOval->FCalibration = 100000;
		
		if(500 < GChannel[i].FOval->FMeasurementTime)				// TI: measurement time checking.
			GChannel[i].FOval->FMeasurementTime = 500;
		if(60 > GChannel[i].FOval->FMeasurementTime)				
			GChannel[i].FOval->FMeasurementTime = 60;
	}
	
	if(GOvalBuffSize < 1)
		GOvalBuffSize = 1;
	if(GOvalBuffSize > 512)
		GOvalBuffSize = 512;
	
	if(GFanON_Temperature < 30)
		GFanON_Temperature = 30;
	if(GFanON_Temperature > 60)
		GFanON_Temperature = 60;
}



/***********************@ApplicationInit
  *		@brief  Default initialization.
  *  	@param  None
  *  	@retval None
  **********************/
static void ApplicationInit(void)
{
	GMeasurementsON = 0;		
	
	for(uint8_t i=0; i<VALVE_CNT; i++)
	{
		GValve[i].FValue = 0;
	}
	
	for(uint8_t i=0; i< CHANNEL_NUM; i++)
	{
		GChannel[i].FChannelName = GChannelNames[i];
		
		GChannel[i].FTemperatureSensor1 = &(ds18b20_devices[i]);
		GChannel[i].FTemperatureSensor2 = &(ds18b20_devices[i+CHANNEL_NUM]);
		
		GChannel[i].FTemperatureSensor1Ready = 0;
		GChannel[i].FTemperatureSensor2Ready = 0;
		GChannel[i].FOutputTemperature1  = 0;
		GChannel[i].FOutputTemperature2  = 0;
		
		GChannel[i].FOval = &(GOval[i]);		
		for(uint8_t j=0; j<UNITS_CNT; j++)
			GChannel[i].FOutputValue[j] = 0;
		GChannel[i].FTopValue = (float)5.0;
		GChannel[i].FUnitId = 0;
		GChannel[i].FChannelError = 0;
		GChannel[i].FMeasureStartTimer = 0;
		
		GChannel[i].Log.curr_line = 0;
		for(uint8_t j=0; j<LOG_LINES_BUFF_SIZE; j++)
			strcpy(GChannel[i].Log.line[j], "");
	}

	
	GTaho.FPulse = 0;
	GTaho.FOutput = 0.0;
	GTaho.FTimeCnr = 0;
	for(uint8_t j=0; j<TAHO_BUF_SIZE; j++)
	{
//		GTaho.FCnrBuffer[j] = 0;
		GTaho.FTimeBuffer[j] = 0;
	}
	GTaho.FBufferPos = 0;
//	GTaho.FTimeBufferPos = 0;
	
	
	GApplicationRun = 0;
	GApplicationMeasurementReady = 0;
	GApplicationStop = 0;
	GValvesMode = DELIVERY_MODE;
	ValvesMode_Prev = BACK_FLOW_MODE;
}



/***********************@CreateModbusTags
  *		@brief  Bind modbus registers with variables in RAM.
  *  	@param  None
  *  	@retval None
  **********************/
static void CreateModbusTags(void)
{
	uint16_t* temp_ptr;
	bool* temp_ptr_bool;
	
	CreateUint16InputTag(&flow_meter_code, 0 ); 
		
	for(uint8_t i = 0; i<CHANNEL_NUM; i++)
	{
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputValue[0]));  // create flow value tag ml/min
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i+1) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i) );
		
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputValue[1]));	// create temperature tag ml/100_str
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 3) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 2) );
		
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputValue[2]));	// create temperature tag ml/200_str
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 5) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 4) );
		
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputValue[3]));	// create temperature tag ml/1000_str
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 7) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 6) );
		
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputValue[4]));	// create temperature tag l/h
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 9) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 8) );
		
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputTemperature1));	// create temperature1 tag
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 11) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 10) );
		
		temp_ptr = (uint16_t*)(&(GChannel[i].FOutputTemperature2));	// create temperature2 tag
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 13) );
		temp_ptr++;
		CreateUint16InputTag(temp_ptr, (uint16_t)(2+14*i + 12) );
		
		temp_ptr_bool = (bool*)(&(GChannel[i].FChannelError) );  		// create channel error tag
		CreateBoolInputTag(temp_ptr_bool, (uint16_t)i);
	}
	
	temp_ptr = (uint16_t*)(&(GTaho.FOutput));  // create taho tag
	CreateUint16InputTag(temp_ptr, 2+ 14*CHANNEL_NUM+1 );
	temp_ptr++;
	CreateUint16InputTag(temp_ptr, 2+ 14*CHANNEL_NUM ); 
	
	temp_ptr = (uint16_t*)(&(GOvalBuffSize));
	CreateUint16Tag(temp_ptr, 0);
	
	temp_ptr = (uint16_t*)(&(GValvesMode));
	CreateUint16Tag(temp_ptr, 1);
	
	GApplicationRun = 0;
	GApplicationMeasurementReady = 0;
	
	temp_ptr_bool = &(GApplicationRun);
	CreateBoolTag(temp_ptr_bool, 0);
	
	temp_ptr_bool = &(GApplicationStop);
	CreateBoolTag(temp_ptr_bool, 1);
	
	temp_ptr_bool = &(GApplicationMeasurementReady);
	CreateBoolInputTag(temp_ptr_bool, 10);
}



/***********************@FlashMapInit
  *		@brief  Bind Flash with variables in RAM.
  *  	@param  None
  *  	@retval None
  **********************/
static uint16_t FlashMapInit(void)
{
	GFlashSavingRegister[0].FTarget = &(GIpAddress.byte1);
	GFlashSavingRegister[1].FTarget = &(GIpAddress.byte2);
	GFlashSavingRegister[2].FTarget = &(GIpAddress.byte3);
	GFlashSavingRegister[3].FTarget = &(GIpAddress.byte4);
	
	GFlashSavingRegister[4].FTarget = &(GNetMask.byte1);
	GFlashSavingRegister[5].FTarget = &(GNetMask.byte2);
	GFlashSavingRegister[6].FTarget = &(GNetMask.byte3);
	GFlashSavingRegister[7].FTarget = &(GNetMask.byte4);
	
	GFlashSavingRegister[8].FTarget  = &(GDefaultGateway.byte1);
	GFlashSavingRegister[9].FTarget  = &(GDefaultGateway.byte2);
	GFlashSavingRegister[10].FTarget = &(GDefaultGateway.byte3);
	GFlashSavingRegister[11].FTarget = &(GDefaultGateway.byte4);
	
	for(uint8_t i=0; i<CHANNEL_NUM; i++)
	{
		GFlashSavingRegister[12+i*2].FTarget = &(GChannel[i].FOval->FCalibration);
		GFlashSavingRegister[13+i*2].FTarget = &(GChannel[i].FOval->FMeasurementTime);		// TI: 06.09.16
	}
	
	GFlashSavingRegister[2*CHANNEL_NUM + 12].FTarget = &(GFanON_Temperature);
	
	for(uint8_t i=0; i<MAXDS18B20HASHSIZE; i++)
	{
		GFlashSavingRegister[2*CHANNEL_NUM + 2*i + 13].FTarget = (uint32_t*)(GDS18B20_HashFile[i].FId);
		GFlashSavingRegister[2*CHANNEL_NUM + 2*i + 14].FTarget = (uint32_t*)(GDS18B20_HashFile[i].FId+4);
	}
	
	GFlashSavingRegister[2*CHANNEL_NUM + 2*MAXDS18B20HASHSIZE + 13].FTarget = (uint32_t*)&(GOvalBuffSize);
	
	return 2*CHANNEL_NUM + 2*MAXDS18B20HASHSIZE + 14;
}
 

/**********************************************************************************END OF FILE****/
