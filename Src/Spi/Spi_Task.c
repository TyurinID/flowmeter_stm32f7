

#include "Spi/Spi_Task.h"
#include "Application.h"

#define BUFFERSIZE 256
#define BUFFEREND 255

#define MESSAGESIZE 100

/* Buffer used for transmission */
static uint8_t aTxBuffer[BUFFERSIZE];

/* Buffer used for reception */
static uint8_t aRxBuffer[MESSAGESIZE];


static uint8_t done;



static uint8_t crc8d5(uint8_t *pcBlock, unsigned int len, uint8_t *crc)
{
	unsigned int i;
 
  while (len--)
  {
    *crc ^= *pcBlock++;
 
    for (i = 0; i < 8; i++)
        *crc = *crc & 0x80 ? (*crc << 1) ^ 0xd5 : *crc << 1;
  }
  return *crc;
}



void SPIThread(void const *argument)
{
	uint8_t bit_pos;
	uint8_t byte_pos;
	
	SpiInit();
	
	osDelay(3000);
	
	aTxBuffer[0] = 0xaa;
	//aTxBuffer[1] = 0x02;
	
	for(uint8_t i=0; i<MESSAGESIZE; i++)
	{
		//aTxBuffer[i] = i;
		aRxBuffer[i] = 0;
	}
	
	while(1)
	{
		// refresh tx buffer
		for(uint16_t i=1; i<BUFFERSIZE; i++)  
		{
			aTxBuffer[i] = 0;
		}
		
		// generate tx_buffer
		for(uint8_t i=0; i<VALVE_CNT; i++)
		{
			byte_pos = i/8;
			bit_pos = i%8;
			if(GValve[i].FValue)
			{
				aTxBuffer[1+byte_pos] |= (1UL<<bit_pos);
			}
		}
		
		uint8_t crc = 0;
		crc = crc8d5(aTxBuffer, MESSAGESIZE-1, &crc);
		aTxBuffer[MESSAGESIZE-1] = crc;
		
		
		
		done = 0;
		if(HAL_OK == HAL_SPI_TransmitReceive_IT(&SpiHandle, (uint8_t*)aTxBuffer, (uint8_t *)aRxBuffer, MESSAGESIZE))
		{
			while (HAL_SPI_GetState(&SpiHandle) != HAL_SPI_STATE_READY && 0 == done)
			{
				osDelay(10);
			}

			// sycronize frame
			uint8_t offset;
			for(offset = 0; offset<4; offset++)
			{
				if(0xbb == aRxBuffer[offset])
					break;
			}
			// check crc
			crc=0;  
			crc = crc8d5(aRxBuffer+offset, MESSAGESIZE-4, &crc);
			if(crc == aRxBuffer[offset+MESSAGESIZE-4] && 0xbb == aRxBuffer[offset])  
			{
				// process valid cmd
				for(uint8_t j =0; j<OVAL_SENSORS_CNR; j++)
				{
					GOval[j].FPulse = aRxBuffer[offset+1+j];
					GOval[j].FTimes_cnr = 					(((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*j])<<24UL) +
																					(((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*j+1])<<16UL) +
																					(((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*j+2])<<8UL) +
																					((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*j+3]);
				}
				GTaho.FPulse = aRxBuffer[offset+1+OVAL_SENSORS_CNR];
				GTaho.FTimeCnr = (((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*OVAL_SENSORS_CNR])<<24UL) +
																					(((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*OVAL_SENSORS_CNR+1])<<16UL) +
																					(((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*OVAL_SENSORS_CNR+2])<<8UL) +
																					((uint32_t)aRxBuffer[offset+2+OVAL_SENSORS_CNR+4*OVAL_SENSORS_CNR+3]);
				
			}
		}
		
		osDelay(50);
	}
}


/**
  * @brief  TxRx Transfer completed callback.
  * @param  hspi: SPI handle
  * @note   This example shows a simple way to report end of Interrupt TxRx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	done = 1;
}




